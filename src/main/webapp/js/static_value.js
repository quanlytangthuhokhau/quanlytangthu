const urlRoot = 'http://localhost:8080/';

var getJson = function (uri) {
    var dataUrl = urlRoot + uri;
    return $.ajax(
        {
            url: dataUrl ,
            method: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        }
    );
};

var postJson = function (uri, object) {
    var dataUrl = urlRoot + uri;
    return $.ajax(
        {
            url: dataUrl ,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object)
        }
    );
};

var putJson = function (uri, object) {
    var dataUrl = urlRoot + uri;
    return $.ajax(
        {
            url: dataUrl ,
            method: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object)
        }
    );
};

var deleteJson=function (uri,object) {
    var dataUrl = urlRoot + uri;
    return $.ajax(
        {
        url: dataUrl ,
        method: "DELETE",
        dataType: "json",
        contentType: "application/json",
            data: JSON.stringify(object)
    });
};


