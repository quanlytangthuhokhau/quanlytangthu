var record1 = [];

function getRecord1() {
    getJson("create_family/getrecord/").done(
        function (data) {
            record1 = [];
            $.each(data, function (index, value) {
                record1.push(value);
            });
            searchRecord1();
        }
    );
}

function getGoodMan1(record) {
    getJson("create_family/allfamilybyrecord/" + record).done(
        function (data) {
            $('#InputGoodman1').html("");
            $.each(data.content, function (index, value) {
                $('#InputGoodman1').append(
                    "<option value='" + value.id + "'>" + value.goodman + "</option>"
                );
            });
        }
    );
}

function searchRecord1() {
    $("#InputRecord1").on('keyup click', function () {
        var InputRecord1 = $('#InputRecord1').val();
        if (record1.includes(InputRecord1)) {
            $("#InputRecord1").autocomplete({
                source: record1,
                minLength: 1,
                autoFocus: true,
                focus: function (event, ui) {
                    getGoodMan1(ui.item.value);
                }
            }).on('autocompletechange', function () {
                if ($(this).data('ui-autocomplete').selectedItem === null) {
                    $(this).data('ui-autocomplete').menu.element.children('li:first').children('a').trigger('click');
                }
            });
        } else {
            $("#InputGoodman1").html("");
        }
    });

    $("#InputRecord1").on('keypress keydown', function (e) {
        var InputRecord1 = $('#InputRecord1').val();
        if (e.which === 13 || e.which === 9) {
            if (record1.includes(InputRecord1)) {
                getGoodMan1(InputRecord1);
            } else {
                $("#InputGoodman1").html("");
            }
        }
    });
}

function getPerson(record, goodman, family_id, num) {
    getJson("slice_two_family/getperson/" + record + "/" + goodman + "/" + family_id).done(
        function (data) {
            $('#listPerson' + num).html("");
            $.each(data, function (index, value) {
                $('#listPerson' + num).append(
                    "<tr>" +
                    "<td>" + value.id + "</td>" +
                    "<td>" + value.family.page + "</td>" +
                    "<td>" + value.family.record + "</td>" +
                    "<td>" + value.fullName + "</td>" +
                    "<td>" + value.birthDay.toString().replace(/,/g, '-') + "</td>" +
                    "<td>" + value.family.goodman + "</td>" +
                    "<td>" + value.relationship.name + "</td>" +
                    "<td>" + value.family.village.name + "</td>" +
                    "                            <td><select class='form-control note" + num + "' onclick='changeRelationship(this, " + num + ")'>\n" +
                    "                                <option value=\"0\">Không</option>\n" +
                    "                                <option value=\"1\">Chuyển</option>\n" +
                    "                                <option value=\"2\">Chuyển Kèm Theo</option>\n" +
                    "                            </select></td>" +
                    "</tr>"
                );
            });
        }
    );
}

function getPersonList1(num) {
    $(".changeColumn1").remove();
    var record = $("#InputRecord1").val(),
        goodman = $("#InputGoodman1 option:selected").text(),
        family_id = $("#InputGoodman1").val();
    getPerson(record, goodman, family_id, num);
}

var move1 = [],
    moveAttached1 = [],
    move2 = [],
    moveAttached2 = [];

function getMove1() {
    move1 = [];
    moveAttached1 = [];
    $('.note1').each(function () {
        if (this.value === "1")
            move1.push($(this).parents("tr").children("td:first").html());
        if (this.value === "2")
            moveAttached1.push($(this).parents("tr").children("td:first").html());
    });
    if (move1.length === 0) {
        if (move2.length === 0)
            $('#listPerson').html("");
        else
            getPersonList(move2.concat(moveAttached2));
    } else {
        if (move2.length === 0)
            getPersonList(move1.concat(moveAttached1));
        else
            getPersonList(move1.concat(moveAttached1, move2, moveAttached2));
    }
}

var relation = "";

function getPersonList(ids) {
    relation = "";
    getJson("slice_two_family/getrelationship/").done(
        function (data) {
            $.each(data, function (index, value) {
                relation += "<option value='" + value.id + "'>" + value.name + "</option>";
            });
            getJson("slice_two_family/getpersonbyid/" + ids).done(
                function (data) {
                    $('#listPerson').html("");
                    $.each(data, function (index, value) {
                        $('#listPerson').append(
                            "<tr>" +
                            "<td>" + value.id + "</td>" +
                            "<td>" + value.family.page + "</td>" +
                            "<td>" + value.family.record + "</td>" +
                            "<td>" + value.fullName + "</td>" +
                            "<td>" + value.birthDay.toString().replace(/,/g, '-') + "</td>" +
                            "<td>" + value.family.goodman + "</td>" +
                            "                            <td><select class='form-control'>\n" +
                            relation +
                            "                            </select></td>" +
                            "<td>" + value.family.village.name + "</td>" +
                            "</tr>"
                        )
                        ;
                    });
                }
            );
        }
    );
}

var record2 = [];

function getRecord2() {
    getJson("create_family/getrecord/").done(
        function (data) {
            record2 = [];
            $.each(data, function (index, value) {
                record2.push(value);
            });
            searchRecord2();
        }
    );
}

function searchRecord2() {
    $("#InputRecord2").on('keyup click', function () {
        var InputRecord2 = $('#InputRecord2').val();
        if (record2.includes(InputRecord2)) {
            $("#InputRecord2").autocomplete({
                source: record2,
                minLength: 1,
                autoFocus: true,
                focus: function (event, ui) {
                    getGoodMan2(ui.item.value);
                }
            }).on('autocompletechange', function () {
                if ($(this).data('ui-autocomplete').selectedItem === null) {
                    $(this).data('ui-autocomplete').menu.element.children('li:first').children('a').trigger('click');
                }
            });
        } else {
            $("#InputGoodman2").html("");
        }
    });

    $("#InputRecord2").on('keypress keydown', function (e) {
        var InputRecord2 = $('#InputRecord2').val();
        if (e.which === 13 || e.which === 9) {
            if (record2.includes(InputRecord2)) {
                getGoodMan2(InputRecord2);
            } else {
                $("#InputGoodman2").html("");
            }
        }
    });
}

function getGoodMan2(record) {
    getJson("create_family/allfamilybyrecord/" + record).done(
        function (data) {
            $('#InputGoodman2').html("");
            $.each(data.content, function (index, value) {
                $('#InputGoodman2').append(
                    "<option value='" + value.id + "'>" + value.goodman + "</option>"
                );
            });
        }
    );
}

function getPersonList2(num) {
    $(".changeColumn2").remove();
    var record = $("#InputRecord2").val(),
        goodman = $("#InputGoodman2 option:selected").text(),
        family_id = $("#InputGoodman2").val();
    getPerson(record, goodman, family_id, num);
}

function getMove2() {
    move2 = [];
    moveAttached2 = [];
    $('.note2').each(function () {
        if (this.value === "1")
            move2.push($(this).parents("tr").children("td:first").html());
        if (this.value === "2")
            moveAttached2.push($(this).parents("tr").children("td:first").html());
    });
    if (move2.length === 0) {
        if (move1.length === 0)
            $('#listPerson').html("");
        else
            getPersonList(move1.concat(moveAttached1));
    } else {
        if (move1.length === 0)
            getPersonList(move2.concat(moveAttached2));
        else
            getPersonList(move1.concat(moveAttached1, move2, moveAttached2));
    }
}

function sliceTwoFamily() {
    // $("#listPerson tr").each(function() {
    //     console.log(this);
    // });
    if (confirm("Tách Hộ Thành Công")) {
        var count = 0;
        $('#listPerson tr').each(function () {
            if ($(this).find('td:eq(6)').find('option:selected').text() === "Chủ Hộ") {
                count++;
            }
        });
        if (count === 1) {
            $('#listPerson tr').each(function () {
                var seleted = $(this).find('td:eq(6)').find('option:selected').text();
                var idPerson = $(this).find('td:eq(0)').text();
                if (seleted === "Chủ Hộ") {
                    var id;
                    getJson("slice_two_family/getpersonbyid/" + idPerson).done(
                        function (data) {
                            $.each(data, function (index, value) {
                                id = value.family.village.id;
                            });
                        }
                    );
                    var goodman = $(this).find('td:eq(3)').text(),
                        page = $("#InputPage").val(),
                        record = $("#InputRecord").val();
                    setTimeout(function () {
                        var family = {
                            "page": page,
                            "record": record,
                            "goodman": goodman,
                            "village": {
                                "id": id
                            }
                        };
                        postJson("create_family/family/", family).always(
                            function () {
                                getJson("create_family/family/" + page + "/" + record + "/" + goodman + "/" + id).done(
                                    function (data) {
                                            idFamilyByFull = data.id;
                                    }
                                ).then(function () {
                                    editedRelationship(idFamilyByFull, 1, idPerson);
                                });

                            }
                        );
                    }, 500);
                } else {
                    var seleted = $(this).find('td:eq(6)').find('option:selected').val();
                    setTimeout(function () {
                        editedRelationship(idFamilyByFull, seleted, idPerson);
                    }, 1000);
                }
            });
            var nextInputPage = parseInt($("#InputPage").val()) + 1,
                nextInputRecord = parseInt($("#InputRecord").val()) + 1;
            $("#InputPage").val(nextInputPage);
            $("#InputRecord").val(nextInputRecord);
            $('#listPerson').html("");
            $('#listPerson1').html("");
            $('#listPerson2').html("");
            $("#InputGoodman1").html("");
            $("#InputRecord1").val("");
            $("#InputGoodman1").html("");
            $("#InputRecord2").val("");
        } else
            alert("Chỉ chọn một chủ hộ duy nhất")
    } else
        return false;
}

var idFamilyByFull;

// function getFamilyByFull(page, record, goodman, village_id) {
//     getJson("create_family/family/" + page + "/" + record + "/" + goodman + "/" + village_id).done(
//         function (data) {
//             $.each(data, function (index, value) {
//                 idFamilyByFull = value.id;
//             });
//         }
//     );
// }

function changeRelationship(x, n) {
    if ($('#listPerson' + n + ' tr').length > 1) {
        var relationship = $(x).parents("tr").children("td:eq(6)").html();
        if ((x.value === "1" || x.value === "2") && relationship === "Chủ Hộ") {
            relation = "";
            getJson("slice_two_family/getrelationship/").done(
                function (data) {
                    $.each(data, function (index, value) {
                        relation += "<option value='" + value.id + "'>" + value.name + "</option>";
                    });
                });
            setTimeout(function () {
                if ($(x).parents("tbody").prev().find("th:eq(9)").html() === undefined) {
                    $(x).parents("tbody").prev().find("tr").append('<th scope="col" class="changeColumn' + n + '">Đổi Quan Hệ</th>');
                    $('#listPerson' + n + ' tr').not($(x).parents("tr")).each(function () {
                        $(this).append(
                            "                            <td class='changeColumn" + n + "'><select class='form-control'>\n" +
                            relation +
                            "                            </select></td>");
                    })
                }
            }, 100);

        } else if (x.value === "0" && relationship === "Chủ Hộ") {
            $(".changeColumn" + n).remove();
        }
    }
}

function editedRelationship(family_id, relationship_id, id) {
    // var seleted = $(this).find('td:eq(6)').find('option:selected').val();
    $.ajax({
        url: "http://localhost:8080/slice_two_family/person/" + id,
        method: "PUT",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            family: {
                id: family_id
            },
            relationship: {
                id: relationship_id
            }
        })
    })
}