class Create_People{
    static getListPageOfFamily(pageId, familyId){
        $(pageId).html("");
        var first =true,
            thisClass = this;
        getJson("create_people/get_page").done(
            function (data) {
                $.each(data,
                    function (page, show) {
                        $(pageId).append(
                            "<option value='"+page+"'>"+ show +"</option>"
                        );
                        if (first){
                            thisClass.getListFamilyByPage(familyId,page);
                            first = false;
                        }
                    }
                );
            }
        );
    }
    getListEthnic(idEthnic){
        $(idEthnic).html("");
        getJson("create_people/listEthnic").done(
            function (data) {
                $.each(data,
                    function (key, row) {
                        $(idEthnic).append(
                            "<option value='"+row.id+"'>"+ row.name +"</option>"
                        );
                    }
                );
            }
        );
    }
    getListReligion(idReligion){
        $(idReligion).html("");
        getJson("create_people/listReligion").done(
            function (data) {
                $.each(data,
                    function (key, row) {
                        $(idReligion).append(
                            "<option value='"+row.id+"'>"+ row.name +"</option>"
                        );
                    }
                );
            }
        );
    }
    getListRelationship(idRelationship){
        $(idRelationship).html("");
        getJson("create_people/listRelationship").done(
            function (data) {
                $.each(data,
                    function (key, row) {
                        $(idRelationship).append(
                            "<option value='"+row.id+"'>"+ row.name +"</option>"
                        );
                    }
                );
            }
        );
    }

    static getListFamilyByPage(idFamily, valueOfPage){
        $(idFamily).html("");
        getJson("create_people/get_families/" + valueOfPage).done(
            function (data) {
                $.each(data,
                    function (page, show) {
                        $(idFamily).append(
                            "<option value='"+page+"'>"+ show +"</option>"
                        );
                    }
                );
            }
        );
    }

    city(idCity, idDistrict, idGuild) {
        $(idCity).html("");
        var first =true,
            thisClass = this;
        getJson("create_family/city/").done(
            function (data) {
                $.each(data, function (index, value) {
                    $(idCity).append(
                        "<option value='" + value + "'>" + value + "</option>"
                    );
                    if (first){
                        thisClass.district(idDistrict, idGuild, value);
                        first = false;
                    }
                });
            }
        );
    }

    district(idDistrict, idGuild, city) {
        $(idDistrict).html("");
        var first =true,
            thisClass = this;
        getJson("create_family/district/" + city).done(
            function (data) {
                $.each(data, function (index, value) {
                    $(idDistrict).append(
                        "<option value='" + value + "'>" + value + "</option>"
                    );
                    if (first){
                        thisClass.guild(idGuild, value);
                        first = false;
                    }
                });
            }
        );
    }

    guild(idGuiled, district) {
        getJson("create_family/guild/" + district).done(
            function (data) {
                $(idGuiled).html("");
                $.each(data, function (index, value) {
                    $(idGuiled).append(
                        "<option value='" + value + "'>" + value + "</option>"
                    );
                });
            }
        );
    }

    getInfoGoodman(valueIdFamily, idEthnic, idReligion,idRelationship){
        var idRelationshipOfGoodman = 1;
        getJson("create_people/get_goodman/" + valueIdFamily).done(
            function (goodman) {
                $(idRelationship + " option[value=" + idRelationshipOfGoodman + "]").prop('disabled', true);
                var nextRelation = $(idRelationship).children("option:nth-child(2)").val();
                $(idRelationship + " option[value=" + nextRelation + "]").attr('selected', 'selected');
                $(idRelationship).val(nextRelation);
                $(idEthnic + " option[value=" + goodman.ethnic.id + "]").attr('selected', 'selected');
                $(idEthnic).val(goodman.ethnic.id);
                $(idReligion + " option[value=" + goodman.religion.id + "]").attr('selected', 'selected');
                $(idReligion).val(goodman.religion.id);
            }
        ).fail(
            function () {
                $(idRelationship + " option[value=" + idRelationshipOfGoodman + "]").prop('disabled', false);
                var relationshipValue = $(idRelationship).children("option:first-child").val(),
                    ethnicValue =  $(idEthnic).children("option:first-child").val(),
                    religionValue = $(idReligion).children("option:first-child").val();
                $(idEthnic + " option[value=" + ethnicValue + "]").attr('selected', 'selected');
                $(idEthnic).val(ethnicValue);
                $(idReligion + " option[value=" + religionValue + "]").attr('selected', 'selected');
                $(idReligion).val(religionValue);
                $(idRelationship + " option[value=" + relationshipValue + "]").attr('selected', 'selected');
                $(idRelationship).val(relationshipValue);
                console.log(religionValue + ", " + religionValue + ", " + relationshipValue);
            }
        );
    }
    getPerSonOfFamily(valueIdFamily,idTable, page, size){
        var url = "create_people/listPersonOfFamily/" + valueIdFamily + "/" +page + "/" + size;
        getJson(url).done(
            function (data) {
                var bodyOfTable = $(idTable).find("tbody");
                bodyOfTable.html("");
                $.each(data,
                    function (key, row) {
                        bodyOfTable.append(
                            "<tr>" +
                                "<td>" + row.family.page +"</td>" +
                                "<td>" + row.family.record + "</td>" +
                                "<td>" + row.fullName+ "</td>" +
                                "<td>" + row.relationship.name +"</td>" +
                                "<td>" + row.nativeCountry + "</td>" +
                                "<td>" + row.idCard + "</td>" +
                                "<td>" + row.family.village.name + "</td>" +
                                // "<td>" +
                                //     "<button class='btn btn-danger' th:href='@{}'>Sửa</button>" +
                                //     "<button class='btn btn-info' th:href='@{/create_people/"+row.id+"/delete}'>Xóa</button>" +
                                // "</td>" +
                            "</tr>"
                        );
                    }
                );
            }
        );
    }
    paging(valueIdFamily, paging, page, size){
        getJson("create_people/countPersonByFamily/" + valueIdFamily).done(
            function (value) {
                var numberPage = Math.ceil(value / size),
                    previousPage = page - 1,
                    nextPage = page + 1;
                if (previousPage < 0) {
                    $(paging).html(
                        "<li class='page-item disabled'>" +
                        "<a class='page-link' tabindex='-1'>Trang Trước</a>" +
                        "</li>"
                    );
                } else {
                    $(paging).html(
                        "<li class='page-item'>" +
                        "<a class='page-link' tabindex='-1' " +
                        " onclick='transferPage(" + previousPage + ");'>Trang Trước</a>" +
                        "</li>"
                    );
                }
                var show = 0;
                for (var i=0; i<numberPage;i++){
                    show = i + 1;
                    if (i === page)
                        $(paging).append("<li class='page-item active'>" +
                                "<a onclick='transferPage(" + i + ");' class='page-link'>" + show + "</a>" +
                            "</li>");
                    else
                        $(paging).append("<li class='page-item'>" +
                                "<a onclick='transferPage(" + i + ");' class='page-link'>" + show + "</a>" +
                            "</li>");
                }
                if (nextPage<numberPage){
                    $(paging).append("<li class='page-item'>" +
                        "<a onclick='transferPage(" + nextPage + ");' class='page-link'>Next</a>" +
                        "</li>");
                }
                else{
                    $(paging).append("<li class='disabled page-item'>" +
                        "<a onclick='transferPage(" + page + ");' class='page-link'>Next</a>" +
                        "</li>");
                }

            }
        );

    }
    submitForm(person, form, valueIdFamily,idTable, paging, page, size){
        var thisClass= this;
        $.ajax(
            {
                url: urlRoot + "create_people/create",
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(person)
            }
        ).always(
            function () {
                // $(form).find("input[type='text']").html("");
                thisClass.getPerSonOfFamily(valueIdFamily,idTable, page, size);
                thisClass.paging(valueIdFamily, paging, page, size)
            }
        ).done(
            function (data) {
                console.log(data);
                var idRelationshipOfGoodman = "1";
                if (person["relationship"]["id"] === idRelationshipOfGoodman){
                    $("#relationship" + " option[value=" + idRelationshipOfGoodman + "]").prop('disabled', true);
                    var nextRelation = $("#relationship").children("option:nth-child(2)").val();
                    $("#relationship" + " option[value=" + nextRelation + "]").attr('selected', 'selected');
                    $("#relationship").val(nextRelation);
                }
            }
        );
    }
}