class People_Move{
    constructor() {
        this.goodManId = 1;
        this.notRelation = {
            "id": -1,
            "name": "Chưa Có"
        };
    }
    getFamilySource(){
        var family =$("#familySource"),
            recordSource = $("#record-source").val();
        // if (recordSource !== $("#record-transfer").val()){
            family.html("");
            $("#submitTransfer").prop("disabled",true);
            var thisClass= this;
            $.ajax(
                {
                    url: urlRoot + 'move_people/findFamilyByRecord/' ,
                    method: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: recordSource
                }
            ).done(
                function (data) {
                    // family.html("<option value='" + data.id +"'>" + data.goodman + "</option>");
                    family.html(data.goodman);
                    thisClass.getListPeopleSource(data);
                    $("#checkedPersonTransfer").prop("disabled", false);
                }
            ).fail(
                function () {
                    $("#checkedPersonTransfer").prop("disabled", true);
                    family.html("");
                    $("#peopleOfFamily").html("");
                }
            );
        // }
    }

    getListPeopleSource(family){
        var listPerson = [],
            indexOfList = 0,
            checkPerson = [],
            list = $("#peopleOfFamily"),
            thisClass = this,
            checkTransfer = $("#checkedPersonTransfer");


        list.html("");
        getJson("move_people/getAllPerson/" + family.id)
            .done(
            function (data) {
                $.each(data,
                    function (index, person) {
                        listPerson[indexOfList] = person;
                        checkPerson[indexOfList] = true;
                        list.append("<option value='" + indexOfList +"'>" + person.fullName + "</option>");
                        indexOfList++;
                    }
                );
                var transferPersonList = [],
                    check = true,
                    transferInput = $("#record-transfer");
                transferInput.prop("disabled",true);
                checkTransfer.click(
                    function () {
                        var index = list.val();
                        if (check){
                            $("#record-transfer").prop("disabled",false);
                            check = false;
                        }
                        if (checkPerson[index]){
                            var person = listPerson[index],
                                id = "" + person.id;
                            transferPersonList.push({
                                "idPerSon" : id
                            });
                            var note = transferPersonList.length === 1? "Người Đề Nghị Chuyển": "Chuyển Kèm Theo";
                            $("#peopleOfFamily" + " option[value=" + index + "]").prop('disabled', true);
                            var relationName = person.relationship!=null?
                                person.relationship.name: thisClass.notRelation.name,
                                birthDay = "Chưa Nhập";
                            if (person.birthDay != null){
                                birthDay = person.birthDay.dayOfMonth + "/"
                                    + person.birthDay.monthValue + "/"
                                    + person.birthDay.year;
                            }

                            $("#tableFamilySource").append("<tr>" +
                                "<td>" + person.id + "</td>" +
                                "<td>" + person.family.record + "</td>" +
                                "<td>" + person.family.page + "</td>" +
                                "<td>" + person.fullName + "</td>" +
                                "<td>" + birthDay + "</td>" +
                                "<td>" + person.family.goodman  + "</td>" +
                                "<td>" + relationName + "</td>" +
                                "<td>" + person.family.village.name + "</td>" +
                                "<td>" + note +"</td>" +
                                "</tr>"
                            );
                            checkPerson[index] = false;
                        }
                    }
                );
                $("#move-out").click(
                    function () {
                        thisClass.moveOut(transferPersonList,family);
                    }
                );
                transferInput.on('keypress',
                    function (event) {
                        if (event.keyCode === 13){
                            if (checkTransfer.is(":disabled")){
                                event.preventDefault();
                            }
                            else {
                                checkTransfer.prop("disabled", true);
                                $("#tableFamilyTransfer").find("tbody").html("");
                                thisClass.getFamilyTransfer(transferPersonList);
                            }
                        }
                    }
                );
            }
        );
    }

    getFamilyTransfer(listPersonSource){
        var recordTransfer = $("#record-transfer"),
            recordVal = recordTransfer.val(),
            submitTransfer = $("#submitTransfer");

        if (recordVal !== $("#familySource").val()){
            $("#tableFamilyTransfer").find("tbody").html("");
            var thisClass= this,
                familyTransfer = $("#family-transfer");
            $.ajax(
                {
                    url: urlRoot + 'move_people/findFamilyByRecord/' ,
                    method: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: recordVal
                }
            ).done(
                function (data) {
                    // familyTransfer.html("<option value='" + data.id +"'>" + data.goodman + "</option>");
                    familyTransfer.html(data.goodman);
                    console.log("family transfer:" + data.id);
                    submitTransfer.prop("disabled", false);
                    submitTransfer.click(
                        function () {
                            $("#tableFamilySource").find("tbody").html("");
                            submitTransfer.prop("disabled", true);
                            // recordTransfer.val("");
                            thisClass.transferPeople(data, listPersonSource);
                        }
                    );
                }
            ).fail(
                function () {
                    submitTransfer.prop("disabled", true);
                }
            );
        }
    }
    changeRelationForFamily(record){
        if (record !== $("#familySource").val()){
            $("#tableFamilyTransfer").find("tbody").html("");
            var thisClass= this,
                familyTransfer = $("#family-transfer");
            $.ajax(
                {
                    url: urlRoot + 'move_people/findFamilyByRecord/' ,
                    method: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: record
                }
            ).done(
                function (data) {
                    familyTransfer.html("<option value='" + data.id +"'>" + data.goodman + "</option>");
                    thisClass.getListPeopleTransfer(data);
                }
            );
        }
    }
    transferPeople(family, listPersonSource){
        var thisClass = this;
        console.log(JSON.stringify(listPersonSource));

        $.ajax(
            {
                url: urlRoot + "move_people/transferPerson/"+ family.id ,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(listPersonSource)
            }).always(
            function () {
                thisClass.getListPeopleTransfer(family);
            }
        );
    }
    getListPeopleTransfer(family){
        var thisClass = this;
        console.log("Family transfer (test):" + family.id);
        getJson("move_people/getAllPerson/" + family.id).done(
            function (data) {
                var haveGoodman = false;
                $.each(data,
                    function (row, person) {
                        if (person.relationship!=null){
                            if (person.relationship.id === thisClass.goodManId){
                                haveGoodman = true;
                                return false;
                            }
                        }
                    });
                thisClass.getListRelationship(data, family, haveGoodman);
            }
        );
    }

    getListRelationship(listPerson, family, haveGoodman){
        var thisClass = this,
            tableTransfer = $("#tableFamilyTransfer").find("tbody");
        // tableTransfer.html("");
        getJson("create_people/listRelationship").done(
            function (data) {
                var relationList = [];
                // relationList.push(thisClass.notRelation);
                $.each(data,function (index, relation) {
                    if (relation.id === thisClass.goodManId){
                        if (!haveGoodman){
                            relationList.push(relation);
                        }
                    }
                    else {
                        relationList.push(relation);
                    }
                });
                var changeList = [];
                tableTransfer.html("");
                $.each(listPerson,
                    function (index, person) {
                        var idRelationOfPerson = person.relationship == null? thisClass.notRelation.id : person.relationship.id;
                        var selectRelation = thisClass.getSelectOptionRelation(relationList,
                            idRelationOfPerson, index);
                        changeList.push({
                            "personId": "" + person.id,
                            "oldRelation" : "" + idRelationOfPerson,
                            "newRelation" : "" + idRelationOfPerson
                        });
                        var birthDay = "Chưa Nhập";
                        if (person.birthDay != null){
                            birthDay = person.birthDay.dayOfMonth + "/"
                                + person.birthDay.monthValue + "/"
                                + person.birthDay.year;
                        }
                        tableTransfer.append(
                            "<tr>" +
                            "<td>" + person.id + "</td>" +
                            "<td>" + person.family.record + "</td>" +
                            "<td>" + person.family.page + "</td>" +
                            "<td>" + person.fullName + "</td>" +
                            "<td>" + birthDay + "</td>" +
                            "<td>" + person.family.goodman  + "</td>" +
                            "<td>" + selectRelation + "</td>" +
                            "<td>" + person.family.village.name + "</td>" +
                            "<td></td>" +
                            "</tr>"
                        );
                    });
                tableTransfer.find("select").change(
                    function () {
                        var value = $(this).find(":selected").data("value");
                        changeList[value.index].newRelation = "" + value.relation;
                        console.log(changeList[value.index]);
                    }
                );
                $("#change-relation").click(
                    function () {
                        thisClass.changeRelation(changeList,family);
                    }
                );
            }

        );
    }

    getSelectOptionRelation(relationList, idOfRelation, idPerson){
        if (idOfRelation === this.goodManId){
            return "Chủ Hộ";
        }
        else {
            var result = "<select>";
            $.each(relationList,
                function (index, relation) {
                    var valueRelation = "{\"index\" : "+ idPerson + ", \"relation\" : " + relation.id + "}";
                    if (relation.id === idOfRelation){
                        result +=
                            "<option data-value='" + valueRelation + "' selected>" + relation.name + "</option>";
                    }
                    else {
                        result += "<option data-value='" + valueRelation +"'>" + relation.name + "</option>";
                    }

                });
            result += "</select>";
            return result;
        }

    }
    changeRelation(listPerson, family){
        var thisClass = this;
        $.ajax(
            {
                url: urlRoot + "move_people/changeRelation",
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(listPerson)
            }).always(
            function () {
                thisClass.getListPeopleTransfer(family);
            }
        );
    }
    moveOut(personList, family){
        var thisClass = this;
        $.ajax(
            {
                url: urlRoot + "move_people/move_out/" + family.id,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(personList)
            }).always(
            function () {
                thisClass.getListPeopleTransfer(family);
                $("#tableFamilySource").find("tbody").html("");
            }
        );
    }
}