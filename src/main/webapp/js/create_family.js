$(document).ready(function () {
    city();
    $("#InputCity").change(function () {
        var selectedCity = $(this).children("option:selected").val();
        district(selectedCity);
    });
    $("#InputDistrict").change(function () {
        var selectedDistrict = $(this).children("option:selected").val();
        guild(selectedDistrict);
    });

    village();
    religion();
    ethnic();
});

function city() {
    getJson("create_family/city/").done(
        function (data) {
            $('#InputCity').html("");
            $.each(data, function (index, value) {
                $('#InputCity').append(
                    "<option value='" + value + "'>" + value + "</option>"
                );
            });
            district($('#InputCity').val());
        }
    );
}

function district(city) {
    getJson("create_family/district/" + city).done(
        function (data) {
            $('#InputDistrict').html("");
            $.each(data, function (index, value) {
                $('#InputDistrict').append(
                    "<option value='" + value + "'>" + value + "</option>"
                );
            });
            guild($('#InputDistrict').val());
        }
    );
}

function guild(district) {
    getJson("create_family/guild/" + district).done(
        function (data) {
            $('#InputGuild').html("");
            $.each(data, function (index, value) {
                $('#InputGuild').append(
                    "<option value='" + value + "'>" + value + "</option>"
                );
            });
        }
    );
}

function village() {
    getJson("create_family/village/").done(
        function (data) {
            $('#InputLocation').html("");
            $.each(data, function (index, value) {
                $('#InputLocation').append(
                    "<option value='" + value.id + "'>" + value.name + "</option>"
                );
            });
        }
    );
}

function religion() {
    getJson("create_family/religion/").done(
        function (data) {
            $('#InputReligion').html("");
            $.each(data, function (index, value) {
                $('#InputReligion').append(
                    "<option value='" + value.id + "'>" + value.name + "</option>"
                );
            });
        }
    );
}

function ethnic() {
    getJson("create_family/ethnic/").done(
        function (data) {
            $('#InputEthnic').html("");
            $.each(data, function (index, value) {
                $('#InputEthnic').append(
                    "<option value='" + value.id + "'>" + value.name + "</option>"
                );
            });
        }
    );
}


var idVillage;

function openEditFamily(id) {
    idVillage = id;
    family(id);
    $("#editFamily").modal("show");
}

function familyLimit(page) {
    getJson("create_family/familylimit/" + page).done(
        function (data) {
            $('#allFamily').html("");
            $.each(data.content, function (index, value) {
                $('#allFamily').append(
                    "<tr>" +
                    "<td>" + value.id + "</td>" +
                    "<td>" + value.page + "</td>" +
                    "<td>" + value.record + "</td>" +
                    "<td>" + value.goodman + "</td>" +
                    "<td>" + value.village.name + "</td>" +
                    "<td>Chi Tiết</td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='openEditFamily(" + value.id + ")'>Sửa</a></td>" +
                    "</tr>"
                );
            });

            if (page === "?page=0") {
                var totalPage = data.totalPages - 1;
                $('.pagination').html(
                    "                        <li class=\"page-item\">\n" +
                    "                            <span class=\"page-link\">\n" +
                    "                                <span>0</span> /\n" +
                    "                                <span>" + totalPage + "</span>\n" +
                    "                            </span>\n" +
                    "                        </li>\n" +
                    "                        <li class=\"page-item\">\n" +
                    "                            <a class=\"page-link\" href='/create_family?page=1'>Trang Kế</a>\n" +
                    "                        </li>");
            }
        }
    );
}

function editFamily() {
    var editPage = $("#editPage").val(),
        editRecord = $("#editRecord").val(),
        editGoodMan = $("#editGoodMan").val(),
        editVillage = $("#editVillage").val();
    editedFamily(idVillage, editPage, editRecord, editGoodMan, editVillage);
}

function family(id) {
    getJson("create_family/family/" + id).done(
        function (data) {
            $("#editPage").val(data.page);
            $("#editRecord").val(data.record);
            $("#editGoodMan").val(data.goodman);
            getJson("create_family/village/").done(
                function (data2) {
                    $('#editVillage').html("");
                    $.each(data2, function (index, value) {
                        $('#editVillage').append(
                            "<option value='" + value.id + "' " + (data.village.id === value.id ? 'selected' : '') + ">" + value.name + "</option>"
                        );
                    });
                }
            );
        }
    );
}

var allfamilyByRecord = [],
    allfamilyByPage = [],
    allfamilyByPageRecord = [],
    indexRecord,
    indexPage,
    indexPageRecord;

function allByRecord(record, id) {
    allfamilyByRecord = [];
    getJson("create_family/allfamilybyrecord/" + record).done(
        function (data) {
            $.each(data.content, function (index, value) {
                allfamilyByRecord.push(value.id);
            });
            indexRecord = allfamilyByRecord.indexOf(id);
        }
    );
}

function allByPage(page, id) {
    allfamilyByPage = [];
    getJson("create_family/allfamilybypage/" + page).done(
        function (data) {
            $.each(data.content, function (index, value) {
                allfamilyByPage.push(value.id);
            });
            indexPage = allfamilyByPage.indexOf(id);
        }
    );
}

function allByPageRecord(page, record, id) {
    allfamilyByPageRecord = [];
    getJson("create_family/allfamilybypagerecord/" + page + "/" + record).done(
        function (data) {
            $.each(data.content, function (index, value) {
                allfamilyByPageRecord.push(value.id);
            });
            indexPageRecord = allfamilyByPageRecord.indexOf(id);
        }
    );
}

function editedFamily(id, editPage, editRecord, editGoodMan, editVillage) {
    $.ajax({
        url: "http://localhost:8080/create_family/family/" + id,
        method: "PUT",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({
            page: editPage,
            record: editRecord,
            goodman: editGoodMan,
            village: {
                id: editVillage
            }
        }),
        success: function () {
            var inputRecord = $('#searchRecord').val(),
                inputPage = $('#searchPage').val();
            if (inputRecord === "" && inputPage === "") {
                var page = "?page=0";
                if (location.search !== "")
                    page = location.search;
                familyLimit(page);
            } else {
                if (inputRecord !== "" && inputPage !== "") {
                    allByPageRecord(inputPage, inputRecord, id);
                    setTimeout(function () {
                        familyByPageRecord(inputPage, inputRecord + "?page=" + parseInt(indexPageRecord / 5));
                    }, 100);
                } else if (inputRecord !== "") {
                    allByRecord(inputRecord, id);
                    setTimeout(function () {
                        familyByRecord(inputRecord + "?page=" + parseInt(indexRecord / 5));
                    }, 100);
                } else if (inputPage !== "") {
                    allByPage(inputPage, id);
                    setTimeout(function () {
                        familyByPage(inputPage + "?page=" + parseInt(indexPage / 5));
                    }, 100);
                }
            }
        }
    })
}

var page = [];

function getPage() {
    getJson("create_family/getpage/").done(
        function (data) {
            page = [];
            $.each(data, function (index, value) {
                page.push(value);
            });
            searchPage();
        }
    );
}

function familyByPage(inputPage) {
    getJson("create_family/familybypage/" + inputPage).done(
        function (data) {
            $('#allFamily').html("");
            $.each(data.content, function (index, value) {
                $('#allFamily').append(
                    "<tr>" +
                    "<td>" + value.id + "</td>" +
                    "<td>" + value.page + "</td>" +
                    "<td>" + value.record + "</td>" +
                    "<td>" + value.goodman + "</td>" +
                    "<td>" + value.village.name + "</td>" +
                    "<td>Chi Tiết</td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='openEditFamily(" + value.id + ")'>Sửa</a></td>" +
                    "</tr>"
                );
            });
            if (data.last === false) {
                var totalPage = data.totalPages - 1,
                    pageNum = data.pageable.pageNumber;
                if (pageNum === 0) {
                    $('.pagination').html(
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>\n" +
                        "                        <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPage(\"" + data.content[0].page + "?page=1\")'>Trang Kế</button>\n" +
                        "                        </li>");
                } else {
                    var prevPage = pageNum - 1,
                        nextPage = pageNum + 1;
                    $('.pagination').html(
                        " <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPage(\"" + data.content[0].page + "?page=" + prevPage + "\")'>Trang Trước</button>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>\n" +
                        "                        <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPage(\"" + data.content[0].page + "?page=" + nextPage + "\")'>Trang Kế</button>\n" +
                        "                        </li>"
                    );
                }
            } else {
                var totalPage = data.totalPages - 1,
                    pageNum = data.pageable.pageNumber,
                    prevPage = pageNum - 1;
                if (data.totalPages > 1) {
                    $('.pagination').html(
                        " <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPage(\"" + data.content[0].page + "?page=" + prevPage + "\")'>Trang Trước</button>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>"
                    );
                } else
                    $('.pagination').html("");
            }
        }
    );
}

function searchPage() {
    $("#searchPage").autocomplete({
        source: page
    });

    $("#searchPage").on('click keyup keypress keydown', function (e) {
        var inputRecord = $('#searchRecord').val(),
            inputPage = $('#searchPage').val();
        if (e.which === 13 || e.which === 9) {
            if (inputRecord === "") {
                if (page.includes(inputPage)) {
                    familyByPage(inputPage);
                } else if (inputPage === "") {
                    familyLimit("?page=0");
                } else {
                    $('#allFamily').html("<tr>" +
                        "<td align=\"center\" colspan=\"7\">Không Tìm Thấy</td>" +
                        "</tr>");
                    $('.pagination').html("");
                }
            } else {
                if (inputPage === "") {
                    familyByRecord(inputRecord);
                } else {
                    if (record.includes(inputRecord) && page.includes(inputPage)) {
                        familyByPageRecord(inputPage, inputRecord);
                    } else {
                        $('#allFamily').html("<tr>" +
                            "<td align=\"center\" colspan=\"7\">Không Tìm Thấy</td>" +
                            "</tr>");
                        $('.pagination').html("");
                    }
                }
            }
        }
    });
}

var record = [];

function getRecord() {
    getJson("create_family/getrecord/").done(
        function (data) {
            record = [];
            $.each(data, function (index, value) {
                record.push(value);
            });
            searchRecord();
        }
    );
}

function familyByRecord(inputRecord) {
    getJson("create_family/familybyrecord/" + inputRecord).done(
        function (data) {
            $('#allFamily').html("");
            $.each(data.content, function (index, value) {
                $('#allFamily').append(
                    "<tr>" +
                    "<td>" + value.id + "</td>" +
                    "<td>" + value.page + "</td>" +
                    "<td>" + value.record + "</td>" +
                    "<td>" + value.goodman + "</td>" +
                    "<td>" + value.village.name + "</td>" +
                    "<td>Chi Tiết</td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='openEditFamily(" + value.id + ")'>Sửa</a></td>" +
                    "</tr>"
                );
            });
            if (data.last === false) {
                var totalPage = data.totalPages - 1,
                    pageNum = data.pageable.pageNumber;
                if (pageNum === 0) {
                    $('.pagination').html(
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>\n" +
                        "                        <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByRecord(\"" + data.content[0].record + "?page=1\")'>Trang Kế</button>\n" +
                        "                        </li>");
                } else {
                    var prevPage = pageNum - 1,
                        nextPage = pageNum + 1;
                    $('.pagination').html(
                        " <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByRecord(\"" + data.content[0].record + "?page=" + prevPage + "\")'>Trang Trước</button>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>\n" +
                        "                        <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByRecord(\"" + data.content[0].record + "?page=" + nextPage + "\")'>Trang Kế</button>\n" +
                        "                        </li>"
                    );
                }
            } else {
                var totalPage = data.totalPages - 1,
                    pageNum = data.pageable.pageNumber,
                    prevPage = pageNum - 1;
                if (data.totalPages > 1) {
                    $('.pagination').html(
                        " <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByRecord(\"" + data.content[0].record + "?page=" + prevPage + "\")'>Trang Trước</button>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>"
                    );
                } else
                    $('.pagination').html("");
            }
        }
    );
}

function searchRecord() {
    $("#searchRecord").autocomplete({
        source: record
    });

    $("#searchRecord").on('click keyup keypress keydown', function (e) {
        var inputRecord = $('#searchRecord').val(),
            inputPage = $('#searchPage').val();
        if (e.which === 13 || e.which === 9) {
            if (inputPage === "") {
                if (record.includes(inputRecord)) {
                    familyByRecord(inputRecord);
                } else if (inputRecord === "") {
                    familyLimit("?page=0");
                } else {
                    $('#allFamily').html("<tr>" +
                        "<td align=\"center\" colspan=\"7\">Không Tìm Thấy</td>" +
                        "</tr>");
                    $('.pagination').html("");
                }
            } else {
                if (inputRecord === "") {
                    familyByPage(inputPage);
                } else {
                    if (record.includes(inputRecord) && page.includes(inputPage)) {
                        familyByPageRecord(inputPage, inputRecord);
                    } else {
                        $('#allFamily').html("<tr>" +
                            "<td align=\"center\" colspan=\"7\">Không Tìm Thấy</td>" +
                            "</tr>");
                        $('.pagination').html("");
                    }
                }
            }
        }
    });
}

function familyByPageRecord(inputPage, inputRecord) {
    getJson("create_family/familybypagerecord/" + inputPage + "/" + inputRecord).done(
        function (data) {
            $('#allFamily').html("");
            $.each(data.content, function (index, value) {
                $('#allFamily').append(
                    "<tr>" +
                    "<td>" + value.id + "</td>" +
                    "<td>" + value.page + "</td>" +
                    "<td>" + value.record + "</td>" +
                    "<td>" + value.goodman + "</td>" +
                    "<td>" + value.village.name + "</td>" +
                    "<td>Chi Tiết</td>" +
                    "<td><a href='javascript:;' class='btn btn-block btn-primary btn-flat'  onclick='openEditFamily(" + value.id + ")'>Sửa</a></td>" +
                    "</tr>"
                );
            });
            if (data.last === false) {
                var totalPage = data.totalPages - 1,
                    pageNum = data.pageable.pageNumber;
                if (pageNum === 0) {
                    $('.pagination').html(
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>\n" +
                        "                        <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPageRecord(" + data.content[0].page + ',"' + data.content[0].record + "?page=1\")'>Trang Kế</button>\n" +
                        "                        </li>");
                } else {
                    var prevPage = pageNum - 1,
                        nextPage = pageNum + 1;
                    $('.pagination').html(
                        " <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPageRecord(" + data.content[0].page + ',"' + data.content[0].record + "?page=" + prevPage + "\")'>Trang Trước</button>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>\n" +
                        "                        <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPageRecord(" + data.content[0].page + ',"' + data.content[0].record + "?page=" + nextPage + "\")'>Trang Kế</button>\n" +
                        "                        </li>"
                    );
                }
            } else {
                var totalPage = data.totalPages - 1,
                    pageNum = data.pageable.pageNumber,
                    prevPage = pageNum - 1;
                if (data.totalPages > 1) {
                    $('.pagination').html(
                        " <li class='page-item'>\n" +
                        "                            <button class='page-link' onclick='familyByPageRecord(" + data.content[0].page + ',"' + data.content[0].record + "?page=" + prevPage + "\")'>Trang Trước</button>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"page-item\">\n" +
                        "                            <span class=\"page-link\">\n" +
                        "                                <span>" + pageNum + "</span> /\n" +
                        "                                <span>" + totalPage + "</span>\n" +
                        "                            </span>\n" +
                        "                        </li>"
                    );
                } else {
                    if (data.totalElements === 0) {
                        $('#allFamily').html("<tr>" +
                            "<td align=\"center\" colspan=\"7\">Không Tìm Thấy</td>" +
                            "</tr>");
                    }
                    $('.pagination').html("");
                }
            }
        }
    );
}
