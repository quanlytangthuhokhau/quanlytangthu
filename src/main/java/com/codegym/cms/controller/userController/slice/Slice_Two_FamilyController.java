package com.codegym.cms.controller.userController.slice;

import com.codegym.cms.model.Family;
import com.codegym.cms.model.Person;
import com.codegym.cms.model.Relationship;
import com.codegym.cms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/slice_two_family")
public class Slice_Two_FamilyController {

    @Autowired
    private FamilyService familyService;

    @Autowired
    private PersonService personService;

    @Autowired
    private RelationshipService relationshipService;

    @GetMapping(value = "", produces = "application/json;charset=UTF-8")
    public ModelAndView showSliceTwoFamily() {
        String maxRecord = familyService.getMaxRecord();
        String maxPage = familyService.getMaxPage();
        ModelAndView modelAndView = new ModelAndView("/user/slice/slice_two_family");
        modelAndView.addObject("maxRecord", maxRecord);
        modelAndView.addObject("maxPage", maxPage);
        return modelAndView;
    }

    @RequestMapping(value = "/getperson/{record}/{goodman}/{family_id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Person>> getPerson(@PathVariable("record") String record, @PathVariable("goodman") String goodman, @PathVariable("family_id") Long family_id) {
        Iterable<Person> persons = personService.getPerson(record, goodman, family_id);
        if (persons == null) {
            return new ResponseEntity<Iterable<Person>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<Person>>(persons, HttpStatus.OK);
    }

//    =====================================================================

    @RequestMapping(value = "/getpersonbyid/{ids}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Person>> getPersonById(@PathVariable("ids") List<Long> ids) {
        Iterable<Person> persons = personService.getPersonByListId(ids);
        if (persons == null) {
            return new ResponseEntity<Iterable<Person>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<Person>>(persons, HttpStatus.OK);
    }

    @RequestMapping(value = "/getrelationship/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Relationship>> getRelationShip() {
        Iterable<Relationship> relationships = relationshipService.findAllByIsDeletedEquals(0);
        if (relationships == null) {
            return new ResponseEntity<Iterable<Relationship>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<Relationship>>(relationships, HttpStatus.OK);
    }

    @RequestMapping(value = "/getmaxrecord/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getMaxRecord() {
        String maxRecord = familyService.getMaxRecord();
        if (maxRecord == null) {
            return new ResponseEntity<String>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<String>(maxRecord, HttpStatus.OK);
    }

    @RequestMapping(value = "/getmaxpage/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getMaxPage() {
        String maxPage = familyService.getMaxPage();
        if (maxPage == null) {
            return new ResponseEntity<String>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<String>(maxPage, HttpStatus.OK);
    }

    @RequestMapping(value = "/person/{id}", method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Person> editPerson(@PathVariable("id") long id, @RequestBody Person person) {
        Person currentPerson = personService.findById(id);
        if (currentPerson == null) {
            return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
        } else {
            personService.editRelationship(person.getFamily().getId(), person.getRelationship().getId(), id);
            return new ResponseEntity<Person>(currentPerson, HttpStatus.OK);
        }
    }
}