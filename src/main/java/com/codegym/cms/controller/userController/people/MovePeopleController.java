package com.codegym.cms.controller.userController.people;

import com.codegym.cms.model.Family;
import com.codegym.cms.model.Person;
import com.codegym.cms.service.FamilyService;
import com.codegym.cms.service.PersonService;

import com.github.cliftonlabs.json_simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/move_people")
public class MovePeopleController {
    @RequestMapping(value = "/inside", method = RequestMethod.GET)
    public ModelAndView goToMoveInsidePage(){
        return new ModelAndView("/user/people/move-inside-viliage");
    }

    @Autowired
    private FamilyService familyService;
    @RequestMapping(value = "/findFamilyByRecord", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Family> findByRecord(@RequestBody Object object){
        String record = String.valueOf(object);
        Family family = familyService.findByRecord(record);
        if (family==null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(family,HttpStatus.OK);
    }
    @Autowired
    private PersonService personService;
    @RequestMapping(value = "/getAllPerson/{familyId}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Person>> getListPersonOfFamily(@PathVariable("familyId") long familyId){
        List<Person> personList = personService.findPersonByFamily(familyId);
        if (personList==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(personList, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/transferPerson/{familyId}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Void> transferPerson(@PathVariable("familyId") long familyId,
                                               @RequestBody List<JsonObject> listJson){
//        JSONArray listPerson =new JSONArray();
//        JSONObject object = (JSONObject) listJson.get("list");
//        object.toJSONArray(listPerson);
        if (listJson.size()>0){
            boolean saveResult = personService.transferPerson(listJson, familyId);
            if (saveResult)
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @RequestMapping(value = "/changeRelation", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Void> changeRelation(@RequestBody List<JsonObject> listPersonChange){
        if (listPersonChange.size()>0){
            personService.changeRelation(listPersonChange);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/outside", method = RequestMethod.GET)
    public ModelAndView goToMoveOutsidePage(){
        return new ModelAndView("/user/people/move-out-province");
    }

    @RequestMapping(value = "/move_out/{familyId}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Void> movePersonOut(@PathVariable("familyId") long familyId,
            @RequestBody List<JsonObject> listPersonChange){
        if (listPersonChange.size()>0){
            personService.moveOut(listPersonChange,familyId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
