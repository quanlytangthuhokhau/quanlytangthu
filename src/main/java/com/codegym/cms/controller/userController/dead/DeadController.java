package com.codegym.cms.controller.userController.dead;


import com.codegym.cms.model.City;
import com.codegym.cms.model.Family;
import com.codegym.cms.model.Person;
import com.codegym.cms.model.Relationship;
import com.codegym.cms.repository.PersonRepository;

import com.codegym.cms.service.FamilyService;
import com.codegym.cms.service.PersonService;
import com.codegym.cms.service.RelationshipService;
import com.codegym.cms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/dead")
public class DeadController {

    @Autowired
    private UserService userService;
    @Autowired
    private FamilyService familyService;
    @Autowired
    private PersonService personService;
    @Autowired
    private RelationshipService relationshipService;

    @GetMapping("/dead")
    public ModelAndView get_family() {
        List<String> families = familyService.getAllRecord();
        ModelAndView modelAndView = new ModelAndView("/user/dead/dead");
        modelAndView.addObject("families", families);
        return modelAndView;
    }

    @RequestMapping(value = "/goodman/{family}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Family>> getGoodManByFamilies(@PathVariable String family) {
        Iterable<Family> goodMans = familyService.getAllGoodMan(family);
        if (goodMans == null) {
            return new ResponseEntity<Iterable<Family>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Family>>(goodMans, HttpStatus.OK);
    }


    @RequestMapping(value = "/fullname/{family}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Person>> getFullnameByGoodMan(@PathVariable String family) {
        Iterable<Person> persons = personService.getIdPerson(family);

        if (persons == null) {
            return new ResponseEntity<Iterable<Person>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Person>>(persons, HttpStatus.OK);
    }

    @RequestMapping(value = "/persons/{idGoodman}/{idPerson}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Person>> getFullnameByGoodMan(@PathVariable Long idGoodman,@PathVariable Long idPerson) {


        personService.softDelete(LocalDate.now(), "Dan3", idPerson);

        String families = familyService.getDeadFamily(idGoodman);
        System.out.println(families);
        String fullnames = personService.getDeadFullname(idPerson);
        System.out.println(fullnames);

        if( families.equals(fullnames)){
            familyService.softDelete(LocalDate.now(), "Dan3", idGoodman);
        }
        Iterable<Person> persons = personService.getDeadPerson(idPerson);



        if (persons == null) {
            return new ResponseEntity<Iterable<Person>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Person>>(persons, HttpStatus.OK);
    }

    @GetMapping("/reborn/{idGoodman}/{idPerson}")
    public ModelAndView rebornPerson(@PathVariable Long idGoodman,@PathVariable Long idPerson) {
        personService.reborn(idPerson);
        familyService.rebornFamily(idGoodman);
        ModelAndView modelAndView = new ModelAndView("redirect:/dead/dead");
        return modelAndView;
    }


    @RequestMapping(value = "/relationships", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Relationship>> listRelationships() {

        Iterable<Relationship> relationships = relationshipService.findAllByIsDeletedEquals(0);

        if (relationships == null) {
            return new ResponseEntity<Iterable<Relationship>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Relationship>>(relationships, HttpStatus.OK);
    }
}
