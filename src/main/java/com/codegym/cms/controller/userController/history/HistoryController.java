package com.codegym.cms.controller.userController.history;

import com.codegym.cms.model.History;
import com.codegym.cms.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("history")
public class HistoryController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView pageHistory(){
        return new ModelAndView("/user/history/history");
    }
    @Autowired
    private HistoryService historyService;
    @RequestMapping(value = "/getListHistory/{page}/{size}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<History>> getHistory(@PathVariable int size, @PathVariable int page){
        List<History> histories = historyService.findAll(page,size);
        if (histories==null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else {
            return new ResponseEntity<>(histories,HttpStatus.OK);
        }
    }
    @RequestMapping(value = "/getAmountPage/{size}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Integer> getAmountPage(@PathVariable int size){
        int amountPage = historyService.getAmountPage(size);
        return new ResponseEntity<>(amountPage, HttpStatus.OK);
    }
}
