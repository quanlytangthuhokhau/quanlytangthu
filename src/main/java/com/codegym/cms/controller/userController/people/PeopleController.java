package com.codegym.cms.controller.userController.people;

import com.codegym.cms.model.*;
import com.codegym.cms.service.*;
import com.github.cliftonlabs.json_simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/create_people")
public class PeopleController {
    @Autowired
    private FamilyService familyService;
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView goToCreatePage(){
        return new ModelAndView("/user/people/create_people");
    }

    @RequestMapping(value = "/get_page", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<HashMap<String,String>> getPage(){
        List<String> pages = familyService.getPageList();
        HashMap<String,String> listPage = new HashMap<>();
        for (String page : pages) {
            listPage.put(page, "Trang " + page);
        }
        return new ResponseEntity<>(listPage, HttpStatus.OK) ;
    }

    @RequestMapping(value = "/get_families/{page}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<HashMap<Long,String>> getFamiliesByPage(@PathVariable String page){
        Iterable<Family> families = familyService.findByPage(page);
        HashMap<Long,String> listGoodman = new HashMap<>();
        for (Family family : families){
            listGoodman.put(family.getId(),family.getGoodman());
        }
        return new ResponseEntity<>(listGoodman, HttpStatus.OK);
    }

    @RequestMapping(value = "/get_family/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Family> getFamilyById(@PathVariable Long id){
        Family family = familyService.findById(id);
        if (family == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(family, HttpStatus.OK);
        }
    }
    @Autowired
    private EthnicService ethnicService;
    @RequestMapping(value = "/listEthnic",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Ethnic>> getEthnic(){
        return new ResponseEntity<>(ethnicService.findAllByIsDeletedEquals(0), HttpStatus.OK);
    }
    @Autowired
    private ReligionService religionService;
    @RequestMapping(value = "/listReligion", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Religion>> getReligion(){
        return new ResponseEntity<>(religionService.findAllByIsDeletedEquals(0), HttpStatus.OK);
    }

    @Autowired
    private RelationshipService relationshipService;
    @RequestMapping(value = "/listRelationship", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Relationship>> getRelationship(){
        return new ResponseEntity<>(relationshipService.findAllByIsDeletedEquals(0), HttpStatus.OK);
    }
    @Autowired
    private PersonService personService;
    @RequestMapping(value = "/listPersonOfFamily/{idFamily}/{page}/{size}",
            method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Person>>
        getPersonOfFamily(@PathVariable Long idFamily, @PathVariable int size, @PathVariable int page){
        List<Person> personList = personService.findPersonByFamily(idFamily, page, size);
        if (personList==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(personList, HttpStatus.OK);
        }
    }
    @RequestMapping(value = "/countPersonByFamily/{idFamily}",
            method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Integer>
        countPersonByFamily(@PathVariable Long idFamily){
        return new ResponseEntity<>(personService.countPersonByFamily(idFamily), HttpStatus.OK);
    }

    @RequestMapping(value = "/get_goodman/{idFamily}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Person> getGoodMan(@PathVariable Long idFamily){
        Family family = familyService.findById(idFamily);
        Relationship relationship = relationshipService.findById((long) 1);
        Person goodman = personService.findFirstPerson(family,relationship);
        if (goodman == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else{
            return new ResponseEntity<>(goodman, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> createPerson(@RequestBody JsonObject person){
        personService.createPerson(person);
        return new ResponseEntity<>("Create Success",HttpStatus.OK);
    }

//    @RequestMapping(value = "/edit", method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
//    public ResponseEntity<Void> editPerson(@RequestBody Person person){
//        personService.edit(person);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

}
