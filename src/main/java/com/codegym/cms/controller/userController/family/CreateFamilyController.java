package com.codegym.cms.controller.userController.family;

import com.codegym.cms.model.*;
import com.codegym.cms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/create_family")
public class CreateFamilyController {

    @Autowired
    private FamilyService familyService;

    @Autowired
    private CityService cityService;

    @Autowired
    private VillageService villageService;

    @Autowired
    private ReligionService religionService;

    @Autowired
    private EthnicService ethnicService;

    @Autowired
    private PersonService personService;

    @GetMapping(value = "", produces = "application/json;charset=UTF-8")
    public ModelAndView showCreateFamily(Pageable pageable) {
        Page<Family> families = familyService.findAllByIsDeletedEquals(0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));

        ModelAndView modelAndView = new ModelAndView("/user/family/createFamily");
        modelAndView.addObject("families", families);
        return modelAndView;
    }

    @PostMapping(value = "", produces = "application/json;charset=UTF-8")
    public ModelAndView createFamily(@ModelAttribute("family") Family family, @ModelAttribute("person") Person person) {
        familyService.create(family.getPage(), family.getRecord(), family.getGoodman(), family.getVillage().getId(), LocalDate.now(), "Dan");
        Long id = familyService.findIdFamily(family.getPage(), family.getRecord(), family.getGoodman(), family.getVillage().getId());
        personService.create(family.getGoodman(), 1L, person.getSex(), person.getBirthDay(), person.getIdCard(), person.getPassport(), person.getNativeCountry().replace(",", " - "), person.getEthnic().getId(), person.getBusiness(), person.getPhone(), id, person.getReligion().getId(), LocalDate.now(), "Dan");
        return new ModelAndView("redirect:/create_family");
    }

    @RequestMapping(value = "/city/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> city() {
        List<String> citys = cityService.city();
        if (citys == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(citys, HttpStatus.OK);
    }

    @RequestMapping(value = "/district/{city}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> district(@PathVariable("city") String city) {
        List<String> districts = cityService.district(city);
        if (districts == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(districts, HttpStatus.OK);
    }

    @RequestMapping(value = "/guild/{district}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> guild(@PathVariable("district") String district) {
        List<String> guilds = cityService.guild(district);
        if (guilds == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(guilds, HttpStatus.OK);
    }

    @RequestMapping(value = "/village/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Village>> village() {
        Iterable<Village> villages = villageService.findAllByIsDeletedEquals(0);
        if (villages == null) {
            return new ResponseEntity<Iterable<Village>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<Village>>(villages, HttpStatus.OK);
    }

    @RequestMapping(value = "/religion/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Religion>> religion() {
        Iterable<Religion> religions = religionService.findAllByIsDeletedEquals(0);
        if (religions == null) {
            return new ResponseEntity<Iterable<Religion>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<Religion>>(religions, HttpStatus.OK);
    }

    @RequestMapping(value = "/ethnic/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<Ethnic>> ethnic() {
        Iterable<Ethnic> ethnics = ethnicService.findAllByIsDeletedEquals(0);
        if (ethnics == null) {
            return new ResponseEntity<Iterable<Ethnic>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<Ethnic>>(ethnics, HttpStatus.OK);
    }

    @RequestMapping(value = "/family/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Family> getFamily(@PathVariable("id") long id) {
        Family family = familyService.findById(id);
        if (family == null) {
            return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Family>(family, HttpStatus.OK);
    }

    @RequestMapping(value = "/family/{id}", method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Family> editFamily(@PathVariable("id") long id, @RequestBody Family family) {
        Family currentFamily = familyService.findById(id);
        if (currentFamily == null) {
            return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
        } else {
            familyService.edit(family.getPage(), family.getRecord(), family.getGoodman(), family.getVillage().getId(), LocalDate.now(), "Dan", id);
            return new ResponseEntity<Family>(currentFamily, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/familylimit/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> familyLimit(Pageable pageable) {
        Page<Family> families = familyService.findAllByIsDeletedEquals(0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));

        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

    @RequestMapping(value = "/familybypage/{page}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> familyByPage(@PathVariable("page") String page, Pageable pageable) {
        Page<Family> families = familyService.findAllByPageAndIsDeleted(page, 0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));
        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

    @RequestMapping(value = "/allfamilybypage/{page}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> allFamilyByPage(@PathVariable("page") String page, Pageable pageable) {
        Page<Family> familiess = familyService.findAllByPageAndIsDeleted(page, 0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));
        long totalElement = familiess.getTotalElements();
        Page<Family> families = familyService.findAllByPageAndIsDeleted(page, 0, PageRequest.of(pageable.getPageNumber(), (int) totalElement, Sort.by("id").descending()));
        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

    @RequestMapping(value = "/getpage/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> getPage() {
        List<String> pages = familyService.getPageList();
        if (pages == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(pages, HttpStatus.OK);
    }

    @RequestMapping(value = "/familybyrecord/{record}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> familyByRecord(@PathVariable("record") String record, Pageable pageable) {
        Page<Family> families = familyService.findAllByRecordAndIsDeleted(record, 0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));
        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

    @RequestMapping(value = "/allfamilybyrecord/{record}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> allFamilyByRecord(@PathVariable("record") String record, Pageable pageable) {
        Page<Family> familiess = familyService.findAllByRecordAndIsDeleted(record, 0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));
        long totalElement = familiess.getTotalElements();
        Page<Family> families = familyService.findAllByRecordAndIsDeleted(record, 0, PageRequest.of(pageable.getPageNumber(), (int) totalElement, Sort.by("id").descending()));
        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

    @RequestMapping(value = "/getrecord/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> getRecord() {
        List<String> records = familyService.getAllRecord();
        if (records == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(records, HttpStatus.OK);
    }

    @RequestMapping(value = "/familybypagerecord/{page}/{record}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> familyByPageRecord(@PathVariable("page") String page, @PathVariable("record") String record, Pageable pageable) {
        Page<Family> families = familyService.findAllByPageAndRecordAndIsDeleted(page, record, 0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));
        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

    @RequestMapping(value = "/allfamilybypagerecord/{page}/{record}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Page<Family>> allFamilyByPageRecord(@PathVariable("page") String page, @PathVariable("record") String record, Pageable pageable) {
        Page<Family> familiess = familyService.findAllByPageAndRecordAndIsDeleted(page, record, 0, PageRequest.of(pageable.getPageNumber(), 5, Sort.by("id").descending()));
        long totalElement = familiess.getTotalElements();
        Page<Family> families = familyService.findAllByPageAndRecordAndIsDeleted(page, record, 0, PageRequest.of(pageable.getPageNumber(), (int) totalElement, Sort.by("id").descending()));
        if (families == null) {
            return new ResponseEntity<Page<Family>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Page<Family>>(families, HttpStatus.OK);
    }

//    ========================================================

    @RequestMapping(value = "/family/", method = RequestMethod.POST)
    public ResponseEntity<Void> createFamily(@RequestBody Family family, UriComponentsBuilder ucBuilder) {
        familyService.create(family.getPage(), family.getRecord(), family.getGoodman(), family.getVillage().getId(), LocalDate.now(), "Dan");

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/family/{id}").buildAndExpand(family.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/family/{page}/{record}/{goodman}/{village_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Family> getFamilyByFull(@PathVariable("page") String page, @PathVariable("record") String record, @PathVariable("goodman") String goodman, @PathVariable("village_id") Long village_id) {
        Family family = familyService.getFamilyByFull(page, record, goodman, village_id);
        if (family == null) {
            return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Family>(family, HttpStatus.OK);
    }
}