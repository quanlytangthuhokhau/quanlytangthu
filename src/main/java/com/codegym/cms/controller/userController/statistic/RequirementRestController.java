package com.codegym.cms.controller.userController.statistic;

import com.codegym.cms.model.Person;
import com.github.cliftonlabs.json_simple.JsonObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/requirement")
public class RequirementRestController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView goToMoveInsidePage(){
        return new ModelAndView("/user/statistic/requirement");
    }
}
