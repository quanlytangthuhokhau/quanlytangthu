 package com.codegym.cms.controller.adminController;
 import com.codegym.cms.model.City;
import com.codegym.cms.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;
 import java.util.ArrayList;

 @Controller
@RequestMapping("/admin")
public class CityController {

    @Autowired
    private CityService cityService;


    @GetMapping("/citys")
    public ModelAndView listCitys(@RequestParam(value = "s", required = false) String s) {
        Iterable<City> citys;
            citys = cityService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/city/list");
        modelAndView.addObject("citys", citys);
        return modelAndView;
    }

    @GetMapping("/create-city")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/city/create");
        modelAndView.addObject("city", new City());
        return modelAndView;
    }
    @PostMapping("/create-city")
    public ModelAndView checkValidation(@Valid @ModelAttribute("city") City city, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/city/create");
            return modelAndView;
        } else {
            cityService.create(city.getGuild(), city.getIdDistrict(),city.getDistrict(),city.getIdCity(),city.getCity(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/city/create");
            modelAndView.addObject("city", new City());
            modelAndView.addObject("message", "New city created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-city/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        City city = cityService.findById(id);
        if (city != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/city/edit");
            modelAndView.addObject("city", city);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
    @PostMapping("/edit-city")
    public ModelAndView updateCity(@ModelAttribute("city") City city) {
        cityService.edit(city.getGuild(),city.getIdDistrict(),city.getDistrict(),city.getIdCity(),city.getCity(), LocalDate.now(), "Dan",city.getId());
        
        ModelAndView modelAndView = new ModelAndView("/admin/city/edit");
        modelAndView.addObject("city", city);
        modelAndView.addObject("message", "City updated successfully");
        return modelAndView;
    }

@GetMapping("/delete-city/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        cityService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/citys";
    }
@GetMapping("/view-city/{id}")
    public ModelAndView viewCity(@PathVariable("id") Long id) {
        City city = cityService.findById(id);
        if (city == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/city/view");
        modelAndView.addObject("city", city);
        return modelAndView;}
}