 package com.codegym.cms.controller.adminController;
 import com.codegym.cms.model.Village;
import com.codegym.cms.service.VillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class VillageController {

    @Autowired
    private VillageService villageService;


    @GetMapping("/villages")
    public ModelAndView listVillages(@RequestParam(value = "s", required = false) String s) {
        Iterable<Village> villages;
            villages = villageService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/village/list");
        modelAndView.addObject("villages", villages);
        return modelAndView;
    }

    @GetMapping("/create-village")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/village/create");
        modelAndView.addObject("village", new Village());
        return modelAndView;
    }
    @PostMapping("/create-village")
    public ModelAndView checkValidation(@Valid @ModelAttribute("village") Village village, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/village/create");
            return modelAndView;
        } else {
            villageService.create(village.getName(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/village/create");
            modelAndView.addObject("village", new Village());
            modelAndView.addObject("message", "New village created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-village/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Village village = villageService.findById(id);
        if (village != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/village/edit");
            modelAndView.addObject("village", village);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
    @PostMapping("/edit-village")
    public ModelAndView updateVillage(@ModelAttribute("village") Village village) {
        villageService.edit(village.getName(), LocalDate.now(), "Dan",village.getId());

        ModelAndView modelAndView = new ModelAndView("/admin/village/edit");
        modelAndView.addObject("village", village);
        modelAndView.addObject("message", "Village updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-village/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        villageService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/villages";
    }
    @GetMapping("/view-village/{id}")
    public ModelAndView viewVillage(@PathVariable("id") Long id) {
        Village village = villageService.findById(id);
        if (village == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/village/view");
        modelAndView.addObject("village", village);
        return modelAndView;}
}