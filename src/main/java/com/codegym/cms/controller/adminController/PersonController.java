package com.codegym.cms.controller.adminController;

import com.codegym.cms.model.*;
import com.codegym.cms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private FamilyService familyService;

    @ModelAttribute("familys")
    public Iterable<Family> familys() {
        return familyService.findAllByIsDeletedEquals(0);
    }

    @Autowired
    private EthnicService ethnicService;

    @ModelAttribute("ethnics")
    public Iterable<Ethnic> ethnics() {
        return ethnicService.findAllByIsDeletedEquals(0);
    }

    @Autowired
    private RelationshipService relationshipService;

    @ModelAttribute("relationships")
    public Iterable<Relationship> relationships() {
        return relationshipService.findAllByIsDeletedEquals(0);
    }

    @Autowired
    private ReligionService religionService;

    @ModelAttribute("religions")
    public Iterable<Religion> religions() {
        return religionService.findAllByIsDeletedEquals(0);
    }

    @GetMapping("/persons")
    public ModelAndView listPersons(@RequestParam(value = "s", required = false) String s) {
        Iterable<Person> persons;
        persons = personService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/person/list");
        modelAndView.addObject("persons", persons);
        return modelAndView;
    }

    @GetMapping("/create-person")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/person/create");
        modelAndView.addObject("person", new Person());
        return modelAndView;
    }

    @PostMapping("/create-person")
    public ModelAndView checkValidation(@Valid @ModelAttribute("person") Person person, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/person/create");
            return modelAndView;
        } else {
            personService.create(person.getFullName(), person.getRelationship().getId(), person.getSex(), person.getBirthDay(), person.getIdCard(), person.getPassport(), person.getNativeCountry(), person.getEthnic().getId(), person.getBusiness(), person.getPhone(), person.getFamily().getId(), person.getReligion().getId(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/person/create");
            modelAndView.addObject("person", new Person());
            modelAndView.addObject("message", "New person created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-person/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Person person = personService.findById(id);
        if (person != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/person/edit");
            modelAndView.addObject("person", person);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }

    @PostMapping("/edit-person")
    public ModelAndView updatePerson(@ModelAttribute("person") Person person) {
        personService.edit(person.getFullName(), person.getRelationship().getId(), person.getSex(), person.getBirthDay(), person.getIdCard(), person.getPassport(), person.getNativeCountry(), person.getEthnic().getId(), person.getBusiness(), person.getPhone(), person.getFamily().getId(), person.getReligion().getId(), LocalDate.now(), "Dan", person.getId());

        ModelAndView modelAndView = new ModelAndView("/admin/person/edit");
        modelAndView.addObject("person", person);
        modelAndView.addObject("message", "Person updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-person/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        personService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/persons";
    }

    @GetMapping("/view-person/{id}")
    public ModelAndView viewPerson(@PathVariable("id") Long id) {
        Person person = personService.findById(id);
        if (person == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/person/view");
        modelAndView.addObject("person", person);
        return modelAndView;
    }
}