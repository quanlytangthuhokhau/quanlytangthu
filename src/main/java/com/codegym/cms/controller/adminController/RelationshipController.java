 package com.codegym.cms.controller.adminController;
 import com.codegym.cms.model.Relationship;
import com.codegym.cms.service.RelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class RelationshipController {

    @Autowired
    private RelationshipService relationshipService;

    @GetMapping("/relationships")
    public ModelAndView listRelationships(@RequestParam(value = "s", required = false) String s) {
        Iterable<Relationship> relationships;
            relationships = relationshipService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/relationship/list");
        modelAndView.addObject("relationships", relationships);
        return modelAndView;
    }

    @GetMapping("/create-relationship")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/relationship/create");
        modelAndView.addObject("relationship", new Relationship());
        return modelAndView;
    }
    @PostMapping("/create-relationship")
    public ModelAndView checkValidation(@Valid @ModelAttribute("relationship") Relationship relationship, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/relationship/create");
            return modelAndView;
        } else {
            relationshipService.create(relationship.getName(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/relationship/create");
            modelAndView.addObject("relationship", new Relationship());
            modelAndView.addObject("message", "New relationship created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-relationship/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Relationship relationship = relationshipService.findById(id);
        if (relationship != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/relationship/edit");
            modelAndView.addObject("relationship", relationship);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
    @PostMapping("/edit-relationship")
    public ModelAndView updateRelationship(@ModelAttribute("relationship") Relationship relationship) {
        relationshipService.edit(relationship.getName(), LocalDate.now(), "Dan",relationship.getId());

        ModelAndView modelAndView = new ModelAndView("/admin/relationship/edit");
        modelAndView.addObject("relationship", relationship);
        modelAndView.addObject("message", "Relationship updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-relationship/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        relationshipService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/relationships";
    }
    @GetMapping("/view-relationship/{id}")
    public ModelAndView viewRelationship(@PathVariable("id") Long id) {
        Relationship relationship = relationshipService.findById(id);
        if (relationship == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/relationship/view");
        modelAndView.addObject("relationship", relationship);
        return modelAndView;}
}