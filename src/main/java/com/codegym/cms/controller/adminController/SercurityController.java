package com.codegym.cms.controller.adminController;


import com.codegym.cms.model.User;
import com.codegym.cms.model.UserUpFile;
import com.codegym.cms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.time.LocalDate;

@Controller
@RequestMapping("/user")
public class SercurityController {
    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public ModelAndView loginUser() {
        ModelAndView modelAndView = new ModelAndView("/user/login/login");
        return modelAndView;
    }

    @GetMapping("/changePassword")
    public ModelAndView changePassword(Principal principal) {
//        System.out.println(principal.getName());
        Long id = userService.idSign(principal.getName());
        User user = userService.findById(id);
        if (user != null) {
            UserUpFile userUpFile = new UserUpFile();
            userUpFile.setId(user.getId());
            userUpFile.setPicture(user.getPicture());
            userUpFile.setUsername(user.getUsername());
            userUpFile.setPassword(user.getPassword());
            userUpFile.setEnabled(user.getEnabled());
            userUpFile.setRoles(user.getRoles());

            ModelAndView modelAndView = new ModelAndView("/user/login/change_password");
            modelAndView.addObject("userUpFile", userUpFile);
//            modelAndView.addObject("message", "User updated successfully");

            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }

    }
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
//        System.out.println("Target=" + target);

        if (target.getClass() == UserUpFile.class) {
            // Đăng ký để chuyển đổi giữa các đối tượng multipart thành byte[]
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
    }

    @PostMapping("/changePassword")
    public ModelAndView updateUser(HttpServletRequest request, @ModelAttribute("userUpFile") UserUpFile userUpFile,Principal principal) {
        // Thư mục gốc upload file.
        String uploadRootPath = request.getServletContext().getRealPath("upload");
        //        System.out.println("uploadRootPath=" + uploadRootPath);
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        CommonsMultipartFile[] fileDatas = userUpFile.getFileDatas();
        for (CommonsMultipartFile fileData : fileDatas) {
            // Tên file gốc tại Client.
            String name = fileData.getOriginalFilename();
//            System.out.println("Client File Name = " + name);
            if (name != null && name.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                    // Luồng ghi dữ liệu vào file trên Server.
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    userUpFile.setPicture(uploadRootDir.getName() + File.separator + name);
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                }
            }
            Long id = userService.idSign(principal.getName());

            userService.editUser(principal.getName(), userUpFile.getPassword(), userUpFile.getPicture(), LocalDate.now(), "Dan", id);
            userUpFile.setUsername(principal.getName());

        }
        String picture =userUpFile.getPicture();
        ModelAndView modelAndView = new ModelAndView("/user/login/change_password");
        modelAndView.addObject("userUpFile", userUpFile);
        modelAndView.addObject("picture", picture);
        modelAndView.addObject("message", "Cập nhật thành công");
        return modelAndView;
    }
}