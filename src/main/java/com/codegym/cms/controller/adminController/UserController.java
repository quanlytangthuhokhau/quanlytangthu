 package com.codegym.cms.controller.adminController;
 import com.codegym.cms.model.User;
 import com.codegym.cms.model.UserUpFile;
 import com.codegym.cms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
 import org.springframework.web.bind.WebDataBinder;
 import org.springframework.web.bind.annotation.*;
 import org.springframework.web.multipart.commons.CommonsMultipartFile;
 import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
 import org.springframework.web.servlet.ModelAndView;

 import javax.servlet.http.HttpServletRequest;
 import javax.validation.Valid;
 import java.io.BufferedOutputStream;
 import java.io.File;
 import java.io.FileOutputStream;
 import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/users")
    public ModelAndView listUsers(@RequestParam(value = "s", required = false) String s) {
        Iterable<User> users;
            users = userService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/user/list");
        modelAndView.addObject("users", users);
        return modelAndView;
    }

    @GetMapping("/create-user/{dtp}")
    public ModelAndView showCreateForm(@PathVariable("dtp")Long dtp) {

        ModelAndView modelAndView = new ModelAndView("/admin/user/create");
        if(dtp==0){modelAndView.addObject("message", "New user created successfully");
        }
        modelAndView.addObject("userUpFile", new UserUpFile());
        return modelAndView;
    }
    @PostMapping("/create-user")
    public String checkValidation(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, HttpServletRequest request, @ModelAttribute("sanphamUpFile") UserUpFile userUpFile) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/user/create");
            return "redirect:/admin/create-user";
        } else {

// Thư mục gốc upload file.
            String uploadRootPath = request.getServletContext().getRealPath("upload");
            //        System.out.println("uploadRootPath=" + uploadRootPath);
            File uploadRootDir = new File(uploadRootPath);
            // Tạo thư mục gốc upload nếu nó không tồn tại.
            if (!uploadRootDir.exists()) {
                uploadRootDir.mkdirs();
            }

            CommonsMultipartFile[] fileDatas = userUpFile.getFileDatas();
            for (CommonsMultipartFile fileData : fileDatas) {
                // Tên file gốc tại Client.
                String name = fileData.getOriginalFilename();
//            System.out.println("Client File Name = " + name);
                if (name != null && name.length() > 0) {
                    try {
                        // Tạo file tại Server.
                        File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                        // Luồng ghi dữ liệu vào file trên Server.
                        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                        stream.write(fileData.getBytes());
                        stream.close();
                        user.setPicture(uploadRootDir.getName() + File.separator + name);
                    } catch (Exception e) {
                        System.out.println("Error Write file: " + name);
                    }
                }
                userService.create(user.getUsername(),user.getPassword(),user.getEnabled(),user.getRoles(),user.getPicture(), LocalDate.now(), "Dan");
            }
//            ModelAndView modelAndView = new ModelAndView("/admin/user/create");
//            modelAndView.addObject("user", new User());
//            modelAndView.addObject("message", "New user created successfully");
            return "redirect:/admin/create-user/0";
        }
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
//        System.out.println("Target=" + target);

        if (target.getClass() == UserUpFile.class) {
            // Đăng ký để chuyển đổi giữa các đối tượng multipart thành byte[]
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
    }

    @GetMapping("/edit-user/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        User user = userService.findById(id);
        if (user != null) {
            UserUpFile userUpFile = new UserUpFile();
            userUpFile.setId(user.getId());
            userUpFile.setPicture(user.getPicture());
            userUpFile.setUsername(user.getUsername());
            userUpFile.setPassword(user.getPassword());
            userUpFile.setEnabled(user.getEnabled());
            userUpFile.setRoles(user.getRoles());

            ModelAndView modelAndView = new ModelAndView("/admin/user/edit");
            modelAndView.addObject("userUpFile", userUpFile);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
    @PostMapping("/edit-user")
    public ModelAndView updateUser(HttpServletRequest request,@ModelAttribute("userUpFile") UserUpFile userUpFile) {
        // Thư mục gốc upload file.
        String uploadRootPath = request.getServletContext().getRealPath("upload");
        //        System.out.println("uploadRootPath=" + uploadRootPath);
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        CommonsMultipartFile[] fileDatas = userUpFile.getFileDatas();
        for (CommonsMultipartFile fileData : fileDatas) {
            // Tên file gốc tại Client.
            String name = fileData.getOriginalFilename();
//            System.out.println("Client File Name = " + name);
            if (name != null && name.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                    // Luồng ghi dữ liệu vào file trên Server.
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    userUpFile.setPicture(uploadRootDir.getName() + File.separator + name);
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                }
            }
            userService.edit(userUpFile.getUsername(),userUpFile.getPassword(),userUpFile.getEnabled(),userUpFile.getRoles(),userUpFile.getPicture(), LocalDate.now(), "Dan",userUpFile.getId());
        }

        ModelAndView modelAndView = new ModelAndView("/admin/user/edit");
        modelAndView.addObject("userUpFile", userUpFile);
        modelAndView.addObject("message", "Cập nhật thành công");
        return modelAndView;
    }

    @GetMapping("/delete-user/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        userService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/users";
    }
    @GetMapping("/view-user/{id}")
    public ModelAndView viewUser(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        if (user == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/user/view");
        modelAndView.addObject("user", user);
        return modelAndView;}
}