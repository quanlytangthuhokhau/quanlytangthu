 package com.codegym.cms.controller.adminController;
 import com.codegym.cms.model.Ethnic;
import com.codegym.cms.service.EthnicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class EthnicController {

    @Autowired
    private EthnicService ethnicService;

    @GetMapping("/ethnics")
    public ModelAndView listEthnics(@RequestParam(value = "s", required = false) String s) {
        Iterable<Ethnic> ethnics;
            ethnics = ethnicService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/ethnic/list");
        modelAndView.addObject("ethnics", ethnics);
        return modelAndView;
    }

    @GetMapping("/create-ethnic")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/ethnic/create");
        modelAndView.addObject("ethnic", new Ethnic());
        return modelAndView;
    }
    @PostMapping("/create-ethnic")
    public ModelAndView checkValidation(@Valid @ModelAttribute("ethnic") Ethnic ethnic, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/ethnic/create");
            return modelAndView;
        } else {
            ethnicService.create(ethnic.getName(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/ethnic/create");
            modelAndView.addObject("ethnic", new Ethnic());
            modelAndView.addObject("message", "New ethnic created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-ethnic/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Ethnic ethnic = ethnicService.findById(id);
        if (ethnic != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/ethnic/edit");
            modelAndView.addObject("ethnic", ethnic);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
    @PostMapping("/edit-ethnic")
    public ModelAndView updateEthnic(@ModelAttribute("ethnic") Ethnic ethnic) {
        ethnicService.edit(ethnic.getName(), LocalDate.now(), "Dan",ethnic.getId());

        ModelAndView modelAndView = new ModelAndView("/admin/ethnic/edit");
        modelAndView.addObject("ethnic", ethnic);
        modelAndView.addObject("message", "Ethnic updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-ethnic/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        ethnicService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/ethnics";
    }
    @GetMapping("/view-ethnic/{id}")
    public ModelAndView viewEthnic(@PathVariable("id") Long id) {
        Ethnic ethnic = ethnicService.findById(id);
        if (ethnic == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/ethnic/view");
        modelAndView.addObject("ethnic", ethnic);
        return modelAndView;}
}