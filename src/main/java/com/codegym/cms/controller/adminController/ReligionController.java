package com.codegym.cms.controller.adminController;

import com.codegym.cms.model.Religion;
import com.codegym.cms.service.ReligionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class ReligionController {

    @Autowired
    private ReligionService religionService;


    @GetMapping("/religions")
    public ModelAndView listReligions(@RequestParam(value = "s", required = false) String s) {
        Iterable<Religion> religions;
        religions = religionService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/religion/list");
        modelAndView.addObject("religions", religions);
        return modelAndView;
    }

    @GetMapping("/create-religion")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/religion/create");
        modelAndView.addObject("religion", new Religion());
        return modelAndView;
    }

    @PostMapping("/create-religion")
    public ModelAndView checkValidation(@Valid @ModelAttribute("religion") Religion religion, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/religion/create");
            return modelAndView;
        } else {
            religionService.create(religion.getName(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/religion/create");
            modelAndView.addObject("religion", new Religion());
            modelAndView.addObject("message", "New religion created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-religion/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Religion religion = religionService.findById(id);
        if (religion != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/religion/edit");
            modelAndView.addObject("religion", religion);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }

    @PostMapping("/edit-religion")
    public ModelAndView updateReligion(@ModelAttribute("religion") Religion religion) {
        religionService.edit(religion.getName(), LocalDate.now(), "Dan", religion.getId());

        ModelAndView modelAndView = new ModelAndView("/admin/religion/edit");
        modelAndView.addObject("religion", religion);
        modelAndView.addObject("message", "Religion updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-religion/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        religionService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/religions";
    }

    @GetMapping("/view-religion/{id}")
    public ModelAndView viewReligion(@PathVariable("id") Long id) {
        Religion religion = religionService.findById(id);
        if (religion == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/religion/view");
        modelAndView.addObject("religion", religion);
        return modelAndView;
    }
}