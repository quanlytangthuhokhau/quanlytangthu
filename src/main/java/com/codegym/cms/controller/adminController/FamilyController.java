 package com.codegym.cms.controller.adminController;
 import com.codegym.cms.model.Family;
 import com.codegym.cms.model.Village;
 import com.codegym.cms.service.FamilyService;
 import com.codegym.cms.service.VillageService;
 import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/admin")
public class FamilyController {

    @Autowired
    private FamilyService familyService;

    @Autowired
    private VillageService villageService;
    @ModelAttribute("villages")
    public Iterable<Village> villages() {
        return villageService.findAllByIsDeletedEquals(0);
    }

    @GetMapping("/familys")
    public ModelAndView listFamilys(@RequestParam(value = "s", required = false) String s) {
        Iterable<Family> familys;
            familys = familyService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/admin/family/list");
        modelAndView.addObject("familys", familys);
        return modelAndView;
    }

    @GetMapping("/create-family")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/family/create");
        modelAndView.addObject("family", new Family());
        return modelAndView;
    }
    @PostMapping("/create-family")
    public ModelAndView checkValidation(@Valid @ModelAttribute("family") Family family, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/family/create");
            return modelAndView;
        } else {
            familyService.create(family.getPage(),family.getRecord(),family.getGoodman(),family.getVillage().getId(), LocalDate.now(), "Dan");

            ModelAndView modelAndView = new ModelAndView("/admin/family/create");
            modelAndView.addObject("family", new Family());
            modelAndView.addObject("message", "New family created successfully");
            return modelAndView;
        }
    }

    @GetMapping("/edit-family/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Family family = familyService.findById(id);
        if (family != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/family/edit");
            modelAndView.addObject("family", family);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }
    @PostMapping("/edit-family")
    public ModelAndView updateFamily(@ModelAttribute("family") Family family) {
        familyService.edit(family.getPage(),family.getRecord(),family.getGoodman(),family.getVillage().getId(), LocalDate.now(), "Dan",family.getId());

        ModelAndView modelAndView = new ModelAndView("/admin/family/edit");
        modelAndView.addObject("family", family);
        modelAndView.addObject("message", "Family updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-family/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        familyService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/familys";
    }
    @GetMapping("/view-family/{id}")
    public ModelAndView viewFamily(@PathVariable("id") Long id) {
        Family family = familyService.findById(id);
        if (family == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/admin/family/view");
        modelAndView.addObject("family", family);
        return modelAndView;}


}