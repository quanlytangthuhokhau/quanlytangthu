package com.codegym.cms.config;

import com.codegym.cms.service.*;
import com.codegym.cms.formatter.CityFormatter;
import com.codegym.cms.service.impl.*;
import com.codegym.cms.formatter.EthnicFormatter;
import com.codegym.cms.formatter.FamilyFormatter;
import com.codegym.cms.formatter.PersonFormatter;
import com.codegym.cms.formatter.RelationshipFormatter;
import com.codegym.cms.formatter.ReligionFormatter;
import com.codegym.cms.formatter.UserFormatter;
import com.codegym.cms.formatter.VillageFormatter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.format.FormatterRegistry;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@EnableJpaRepositories("com.codegym.cms.repository")
@ComponentScan("com.codegym.cms")
@EnableSpringDataWebSupport
public class ApplicationConfig implements ApplicationContextAware, WebMvcConfigurer {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Bean
    public SpringResourceTemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(applicationContext);
        templateResolver.setPrefix("/WEB-INF/views");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        return templateEngine;
    }

    @Bean
    public ThymeleafViewResolver viewResolver() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setCharacterEncoding("UTF-8");
        viewResolver.setContentType("text/html; charset=utf-8");
        return viewResolver;
    }


    //JPA configuration
    @Bean
    @Qualifier(value = "entityManager")
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[]{"com.codegym.cms.model"});

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        try {
            URL res = getClass().getClassLoader().getResource("database.properties");
            assert res != null;
            Properties prop = new Properties();
            InputStream inputStream = new FileInputStream(Paths.get(res.toURI()).toFile());
            prop.load(inputStream);
            dataSource.setDriverClassName(prop.getProperty("driver"));
            String urlDatabase = prop.getProperty("type") + prop.getProperty("host") +
                    prop.getProperty("name") + prop.getProperty("config");
            dataSource.setUrl(urlDatabase);
            dataSource.setUsername(prop.getProperty("user"));
            dataSource.setPassword(prop.getProperty("password"));
            return dataSource;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.connection.useUnicode", "true");
        properties.setProperty("hibernate.connection.characterEncoding", "UTF-8");
        properties.setProperty("hibernate.connection.charSet", "UTF-8");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        return properties;
    }


    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new CityFormatter(applicationContext.getBean(CityService.class)));
        registry.addFormatter(new EthnicFormatter(applicationContext.getBean(EthnicService.class)));
        registry.addFormatter(new FamilyFormatter(applicationContext.getBean(FamilyService.class)));
        registry.addFormatter(new PersonFormatter(applicationContext.getBean(PersonService.class)));
        registry.addFormatter(new RelationshipFormatter(applicationContext.getBean(RelationshipService.class)));
        registry.addFormatter(new ReligionFormatter(applicationContext.getBean(ReligionService.class)));
        registry.addFormatter(new UserFormatter(applicationContext.getBean(UserService.class)));
        registry.addFormatter(new VillageFormatter(applicationContext.getBean(VillageService.class)));
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/sb2/**")
                .addResourceLocations("/sb2/").resourceChain(false);

        registry.addResourceHandler("/upload/**")
                .addResourceLocations("/upload/").resourceChain(false);
        registry.addResourceHandler("/js/**")
                .addResourceLocations("/js/").resourceChain(false);
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(1000000);
        return multipartResolver;
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("ValidationMessages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public CityService cityService() {
        return new CityServiceImpl();
    }

    @Bean
    public EthnicService ethnicService() {
        return new EthnicServiceImpl();
    }

    @Bean
    public FamilyService familyService() {
        return new FamilyServiceImpl();
    }

    @Bean
    public PersonService personService() {
        return new PersonServiceImpl();
    }

    @Bean
    public RelationshipService relationshipService() {
        return new RelationshipServiceImpl();
    }

    @Bean
    public ReligionService religionService() {
        return new ReligionServiceImpl();
    }

    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    public VillageService villageService() {
        return new VillageServiceImpl();
    }

    @Bean
    public HistoryService historyService() {
        return new HistoryServiceImpl();
    }
}