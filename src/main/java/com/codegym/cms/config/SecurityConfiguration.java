package com.codegym.cms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomSuccessHandler customSuccessHandler;

    @Autowired
    private DataSource dataSource;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("SELECT username, password, enabled FROM User WHERE username = ? AND isDeleted = 0;")
                .authoritiesByUsernameQuery("SELECT username, roles FROM User WHERE username = ?;")
                .rolePrefix("ROLE_")
                .passwordEncoder(NoOpPasswordEncoder.getInstance());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //khong can dang nhap van vao duoc
                .antMatchers("/sb2/**", "/home", "/about", "/blog", "/contact", "/create-user2").permitAll()
                //dang nhap ms vao xem duoc
                .antMatchers("/survey", "/js/**", "/upload/**", "/shop", "/shopdetail/**", "/dead/persons/**",
                        "/user/changePassword", "/huyen/**", "/phuong/**", "/dead/fullname/**","/dead/dead","/dead/deadOnly","/dead/goodman/**").authenticated()
                //tat ca cac duong dan co quuyen admin ms xem duoc
                .anyRequest().access("hasRole('ADMIN')")
//                .and().formLogin()
                .and().formLogin().loginPage("/user/login").loginProcessingUrl("/login").permitAll()
                .successHandler(customSuccessHandler).usernameParameter("username").passwordParameter("password")
                .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/")
//                .invalidateHttpSession(false)
//                .deleteCookies("JSESSIONID")
//                .and().csrf()
                .and().exceptionHandling().accessDeniedPage("/Access_Denied");

        http.csrf().disable();
    }
}