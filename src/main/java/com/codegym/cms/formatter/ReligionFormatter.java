package com.codegym.cms.formatter;

import com.codegym.cms.model.Religion;
import com.codegym.cms.service.ReligionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class ReligionFormatter implements Formatter<Religion> {

    private ReligionService religionService;

    @Autowired
    public ReligionFormatter(ReligionService religionService) {
        this.religionService = religionService;
    }

    @Override
    public Religion parse(String text, Locale locale) throws ParseException {
        return religionService.findById(Long.parseLong(text));
    }

    @Override
    public String print(Religion object, Locale locale) {
        return null;  }
}
 
