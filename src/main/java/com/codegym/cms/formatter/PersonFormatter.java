package com.codegym.cms.formatter;

import com.codegym.cms.model.Person;
import com.codegym.cms.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class PersonFormatter implements Formatter<Person> {

    private PersonService personService;

    @Autowired
    public PersonFormatter(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public Person parse(String text, Locale locale) throws ParseException {
        return personService.findById(Long.parseLong(text));
    }

    @Override
    public String print(Person object, Locale locale) {
        return null;  }
}
 
