package com.codegym.cms.formatter;

import com.codegym.cms.model.Family;
import com.codegym.cms.service.FamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class FamilyFormatter implements Formatter<Family> {

    private FamilyService familyService;

    @Autowired
    public FamilyFormatter(FamilyService familyService) {
        this.familyService = familyService;
    }

    @Override
    public Family parse(String text, Locale locale) throws ParseException {
        return familyService.findById(Long.parseLong(text));
    }

    @Override
    public String print(Family object, Locale locale) {
        return null;  }
}
 
