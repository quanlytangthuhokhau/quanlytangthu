package com.codegym.cms.formatter;

import com.codegym.cms.model.Relationship;
import com.codegym.cms.service.RelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class RelationshipFormatter implements Formatter<Relationship> {

    private RelationshipService relationshipService;

    @Autowired
    public RelationshipFormatter(RelationshipService relationshipService) {
        this.relationshipService = relationshipService;
    }

    @Override
    public Relationship parse(String text, Locale locale) throws ParseException {
        return relationshipService.findById(Long.parseLong(text));
    }

    @Override
    public String print(Relationship object, Locale locale) {
        return null;  }
}
 
