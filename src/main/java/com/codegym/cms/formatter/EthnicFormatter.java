package com.codegym.cms.formatter;

import com.codegym.cms.model.Ethnic;
import com.codegym.cms.service.EthnicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class EthnicFormatter implements Formatter<Ethnic> {

    private EthnicService ethnicService;

    @Autowired
    public EthnicFormatter(EthnicService ethnicService) {
        this.ethnicService = ethnicService;
    }

    @Override
    public Ethnic parse(String text, Locale locale) throws ParseException {
        return ethnicService.findById(Long.parseLong(text));
    }

    @Override
    public String print(Ethnic object, Locale locale) {
        return null;  }
}
 
