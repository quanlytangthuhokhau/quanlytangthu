package com.codegym.cms.formatter;

import com.codegym.cms.model.Village;
import com.codegym.cms.service.VillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class VillageFormatter implements Formatter<Village> {

    private VillageService villageService;

    @Autowired
    public VillageFormatter(VillageService villageService) {
        this.villageService = villageService;
    }

    @Override
    public Village parse(String text, Locale locale) throws ParseException {
        return villageService.findById(Long.parseLong(text));
    }

    @Override
    public String print(Village object, Locale locale) {
        return null;  }
}
 
