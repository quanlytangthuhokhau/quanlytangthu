package com.codegym.cms.service;

import com.codegym.cms.model.History;

import java.util.List;

public interface HistoryService {
    Iterable<History> findAll();
    void save(History history);
    List<History> findAll(int page, int size);
    int getAmountPage(int size);
}
