package com.codegym.cms.service;

import com.codegym.cms.model.Family;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface FamilyService {
   Family findById(Long id);
   Iterable<Family> findAllByIsDeletedEquals(int isDeleted);
   void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id);

    Page<Family> findAllByIsDeletedEquals(int isDeleted, Pageable pageable);

   void create(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by);


   void edit(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id );

    Long findIdFamily(String page, String record, String goodman, Long village_id);

   List<String> getPageList();

   List<String> getAllRecord();

   Iterable<Family> findByPage(String page);

    Iterable<Family> getAllGoodMan(@Param("record") String record);

    List<String> getAllFamily(@Param("family_id") Long id);

    Family findByRecord(String record);

    Page<Family> findAllByPageAndIsDeleted(String page, int isDeleted, Pageable pageable);

    Page<Family> findAllByRecordAndIsDeleted(String record, int isDeleted, Pageable pageable);

    Page<Family> findAllByPageAndRecordAndIsDeleted(String page, String record, int isDeleted, Pageable pageable);
   List<Long> getIdDeadOnly();

   String getDeadOnly(@Param("id")Long id);
   String getDeadFamily(@Param("id") Long id);

   void rebornFamily(@Param("id") Long id);


    String getMaxRecord();

    String getMaxPage();

    Family getFamilyByFull(@Param("page") String page,@Param("record") String record,@Param("goodman") String goodman,@Param("village_id") Long village_id);
}
