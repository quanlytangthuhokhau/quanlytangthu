package com.codegym.cms.service.impl;

import com.codegym.cms.model.History;
import com.codegym.cms.repository.HistoryRepository;
import com.codegym.cms.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class HistoryServiceImpl implements HistoryService {
    @Autowired
    private HistoryRepository historyRepository;
    @Override
    public Iterable<History> findAll() {
        return historyRepository.findAll();
    }

    @Override
    public void save(History history) {
        historyRepository.save(history);
    }

    @Override
    public List<History> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page,size);
        List<History> histories = historyRepository.getAllByIsDeletedOrderByIdDesc(0, pageable);
        if (histories.size()==0)
            return null;
        return histories;
    }

    @Override
    public int getAmountPage(int size) {
        int amountRow = historyRepository.countAllByIsDeleted(0);
        return (int) Math.ceil(amountRow/size);
    }
}
