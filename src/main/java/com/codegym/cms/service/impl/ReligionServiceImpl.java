package com.codegym.cms.service.impl;

import com.codegym.cms.model.Religion;
import com.codegym.cms.repository.ReligionRepository;
import com.codegym.cms.service.ReligionService;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

public class ReligionServiceImpl implements ReligionService {
    @Autowired
    private ReligionRepository religionRepository;

    @Override
    public Religion findById(Long id) {
        return religionRepository.findById(id).get();
    }

    @Override
    public Iterable<Religion> findAllByIsDeletedEquals(int isDeleted) {
        return religionRepository.findAllByIsDeletedEquals(isDeleted);
    }

    @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id) {
        religionRepository.softDelete(deleted_at, deleted_by, id);
    }


    @Override
    public void create(@Param("name") String name, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by) {
        religionRepository.create(name, created_at, created_by);
    }

    @Override
    public void edit(@Param("name") String name, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id) {
        religionRepository.edit(name, updated_at, updated_by, id);
    }
}