package com.codegym.cms.service.impl;
import com.codegym.cms.model.Ethnic;
import com.codegym.cms.repository.EthnicRepository;
import com.codegym.cms.service.EthnicService;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.LocalDate;
public class EthnicServiceImpl implements EthnicService {
   @Autowired
   private EthnicRepository ethnicRepository;

   @Override
   public Ethnic findById(Long id){
       return ethnicRepository.findById(id).get();
}
   @Override
   public Iterable<Ethnic> findAllByIsDeletedEquals(int isDeleted) {
       return ethnicRepository.findAllByIsDeletedEquals(isDeleted);
   }
   @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id){
       ethnicRepository.softDelete(deleted_at, deleted_by, id);
}


    @Override
    public void create(@Param("name") String name, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by){
       ethnicRepository.create(name, created_at, created_by);}
   @Override
    public void edit(@Param("name") String name, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long   id){
       ethnicRepository.edit(name, updated_at, updated_by, id);
 }
}