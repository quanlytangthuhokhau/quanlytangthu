package com.codegym.cms.service.impl;

import com.codegym.cms.model.Family;
import com.codegym.cms.model.Person;
import com.codegym.cms.repository.FamilyRepository;
import com.codegym.cms.service.FamilyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

public class FamilyServiceImpl implements FamilyService {
    @Autowired
    private FamilyRepository familyRepository;

    @Override
    public Family findById(Long id) {
        return familyRepository.findById(id).get();
    }

    @Override
    public Iterable<Family> findAllByIsDeletedEquals(int isDeleted) {
        return familyRepository.findAllByIsDeletedEquals(isDeleted);
    }

    @Override
    public Page<Family> findAllByIsDeletedEquals(int isDeleted, Pageable pageable) {
        return familyRepository.findAllByIsDeletedEquals(isDeleted, pageable);
    }

    @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id) {
        familyRepository.softDelete(deleted_at, deleted_by, id);
    }


    @Override
    public void create(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by) {
        familyRepository.create(page, record, goodman, village_id, created_at, created_by);
    }

    @Override
    public void edit(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id) {
        familyRepository.edit(page, record, goodman, village_id, updated_at, updated_by, id);
    }

    @Override
    public Long findIdFamily(String page, String record, String goodman, Long village_id) {
        return familyRepository.findIdFamily(page, record, goodman, village_id);
    }

    @Override
    public List<String> getPageList() {
        return familyRepository.getAllPage();
    }

    @Override
    public List<String> getAllRecord() {
        return familyRepository.getAllRecord();
    }

    @Override
    public Iterable<Family> findByPage(String page) {
        return familyRepository.findAllByPageAndIsDeletedEquals(page, 0);
    }

    @Override
    public Page<Family> findAllByPageAndIsDeleted(String page, int isDeleted, Pageable pageable) {
        return familyRepository.findAllByPageAndIsDeleted(page, isDeleted, pageable);
    }

    @Override
    public Page<Family> findAllByRecordAndIsDeleted(String record, int isDeleted, Pageable pageable) {
        return familyRepository.findAllByRecordAndIsDeleted(record, isDeleted, pageable);
    }

    @Override
    public Page<Family> findAllByPageAndRecordAndIsDeleted(String page, String record, int isDeleted, Pageable pageable) {
        return familyRepository.findAllByPageAndRecordAndIsDeleted(page, record, isDeleted, pageable);
    }

    @Override
    public Iterable<Family> getAllGoodMan(String record) {
        return familyRepository.getAllGoodMan(record);
    }

    @Override
    public List<String> getAllFamily(Long id) {
        return familyRepository.getAllFamily(id);
    }

    @Override
    public Family findByRecord(String record) {
        return familyRepository.findFirstByRecordAndIsDeleted(record, 0);
    }

    @Override
    public List<Long> getIdDeadOnly() {
        return familyRepository.getIdDeadOnly();
    }
    @Override
    public String getDeadFamily(Long id) {
        return familyRepository.getDeadFamily(id);
    }

    @Override
    public String getDeadOnly(Long id) {
        return familyRepository.getDeadOnly(id);
    }

    @Override
    public void rebornFamily(Long id) {
        familyRepository.rebornFamily(id);
    }



    @Override
    public String getMaxRecord() {
        return familyRepository.getMaxRecord();
    }

    @Override
    public String getMaxPage() {
        return familyRepository.getMaxPage();
    }

    @Override
    public Family getFamilyByFull(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id) {
        return familyRepository.getFamilyByFull(page, record, goodman, village_id);
    }
}