package com.codegym.cms.service.impl;
import com.codegym.cms.model.Relationship;
import com.codegym.cms.repository.RelationshipRepository;
import com.codegym.cms.service.RelationshipService;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.LocalDate;
public class RelationshipServiceImpl implements RelationshipService {
   @Autowired
   private RelationshipRepository relationshipRepository;

   @Override
   public Relationship findById(Long id){
       return relationshipRepository.findById(id).get();
}
   @Override
   public Iterable<Relationship> findAllByIsDeletedEquals(int isDeleted) {
       return relationshipRepository.findAllByIsDeletedEquals(isDeleted);
   }
   @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id){
       relationshipRepository.softDelete(deleted_at, deleted_by, id);
}


    @Override
    public void create(@Param("name") String name, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by){
       relationshipRepository.create(name, created_at, created_by);}
   @Override
    public void edit(@Param("name") String name, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long   id){
       relationshipRepository.edit(name, updated_at, updated_by, id);
 }
}