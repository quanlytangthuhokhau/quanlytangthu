package com.codegym.cms.service.impl;
import com.codegym.cms.model.*;
import com.codegym.cms.repository.*;
import com.codegym.cms.service.PersonService;
import com.github.cliftonlabs.json_simple.JsonObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.List;

public class PersonServiceImpl implements PersonService {
    private static long relationIdGoodman = 1;
   @Autowired
   private PersonRepository personRepository;

   @Override
   public Person findById(Long id){
       return personRepository.findById(id).get();
}
   @Override
   public Iterable<Person> findAllByIsDeletedEquals(int isDeleted) {
       return personRepository.findAllByIsDeletedEquals(isDeleted);
   }
   @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id){
       personRepository.softDelete(deleted_at, deleted_by, id);
}


    @Override
    public void create(@Param("fullName") String fullName, @Param("relationship_id") Long relationship_id, @Param("sex") int sex, @Param("birthDay") LocalDate birthDay, @Param("idCard") String idCard, @Param("passport") String passport, @Param("nativeCountry") String nativeCountry, @Param("ethnic_id") Long ethnic_id, @Param("business") String business, @Param("phone") String phone, @Param("family_id") Long family_id, @Param("religion_id") Long religion_id, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by){
       personRepository.create(fullName, relationship_id, sex, birthDay, idCard, passport, nativeCountry, ethnic_id, business, phone, family_id, religion_id, created_at, created_by);}
   @Override
    public void edit(@Param("fullName") String fullName, @Param("relationship_id") Long relationship_id, @Param("sex") int sex, @Param("birthDay") LocalDate birthDay, @Param("idCard") String idCard, @Param("passport") String passport, @Param("nativeCountry") String nativeCountry, @Param("ethnic_id") Long ethnic_id, @Param("business") String business, @Param("phone") String phone, @Param("family_id") Long family_id, @Param("religion_id") Long religion_id, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long   id){
       personRepository.edit(fullName, relationship_id, sex, birthDay, idCard, passport, nativeCountry, ethnic_id, business, phone, family_id, religion_id, updated_at, updated_by, id);
 }

    @Override
    public List<Integer> getId(String record,String goodman, String fullName ) {
        return personRepository.getId(record,goodman,fullName);
    }

    @Override
    public Iterable<Person> getIdPerson(String record) {
        return personRepository.getIdPerson(record);
    }

    @Override
    public Iterable<Person> getDeadPerson(Long id) {
        return personRepository.getDeadPerson(id);
    }

    @Override
    public void reborn(Long id) {
        personRepository.reborn(id);
    }


    @Override
    public Person findFirstPerson(Family family, Relationship relationship) {
       if (family.getIsDeleted()!=0){
           return null;
       }
        return personRepository.findFirstByFamilyAndRelationshipAndIsDeleted(family, relationship,0);
    }
    @Autowired
    private FamilyRepository familyRepository;
    @Override
    public List<Person> findPersonByFamily(Long idFamily, int page, int size) {
       Family family = familyRepository.findById(idFamily).orElse(null);
       if ((family==null) || (family.getIsDeleted()!=0)) {
           return null;
       }
       else {
           List<Person> personList =
                   personRepository.findAllByFamilyAndIsDeletedOrderByIdDesc(family,0, PageRequest.of(page,size)).toList();
           if ( personList.size() == 0)
               return null;
           else
               return personList;
       }
    }

    @Override
    public List<Person> findPersonByFamily(Long idFamily) {
        Family family = familyRepository.findById(idFamily).orElse(null);
        if ((family==null) || (family.getIsDeleted()!=0)) {
            return null;
        }
        else {
            List<Person> personList =
                    personRepository.findAllByFamilyAndIsDeletedOrderByIdDesc(family,0);
            if ( personList.size() == 0)
                return null;
            else
                return personList;
        }
    }

    @Override
    public int countPersonByFamily(Long idFamily) {
        Family family = familyRepository.findById(idFamily).orElse(null);
        if ((family==null) || (family.getIsDeleted()!=0)) {
            return 0;
        }
        else {
            return personRepository.countAllByFamily(family);
        }
    }

    @Autowired
    private ReligionRepository religionRepository;
    @Autowired
    private EthnicRepository ethnicRepository;
    @Override
    public void createPerson(JsonObject personJson) {
        LocalDate localDate = LocalDate.now();
        Family family =
                familyRepository.findById(Long.parseLong((String) personJson.get("family"))).orElse(null);
        Relationship relationship =
                relationshipRepository.findById(Long.parseLong((String) personJson.get("relationship"))).orElse(null);
        Religion religion =
                religionRepository.findById(Long.parseLong((String) personJson.get("religion"))).orElse(null);
        Ethnic ethnic =
                ethnicRepository.findById(Long.parseLong((String) personJson.get("ethnic"))).orElse(null);
        if (family!=null){
            Person person = new Person();
            person.setFullName((String)personJson.get("fullName"));
            person.setSex(Integer.parseInt((String)personJson.get("sex")));
            person.setBirthDay(LocalDate.parse((String) personJson.get("birthDay")));
            person.setIdCard((String) personJson.get("idCard"));
            person.setNativeCountry((String) personJson.get("nativeCountry"));
            person.setBusiness((String) personJson.get("business"));
            person.setPhone((String) personJson.get("phone"));
            person.setFamily(family);
            person.setRelationship(relationship);
            person.setReligion(religion);
            person.setCreated_at(localDate);
            personRepository.save(person);
            if (person.getRelationship().getId() == PersonServiceImpl.relationIdGoodman) {
                family.setUpdated_at(localDate);
                family.setGoodman(person.getFullName());
                familyRepository.save(family);
            }
            this.writeHistory(localDate, person, family, "Thêm Nhân Khẩu Vào Hộ" + family.getRecord());
        }

    }

    @Autowired
    private RelationshipRepository relationshipRepository;
    @Autowired
    private HistoryRepository historyRepository;
    private void writeHistory(LocalDate localDate, Person person, Family family, String note){
        History history = new History();
        history.setRecord(family.getRecord());
        history.setGoodman(family.getGoodman());
        history.setFullName(person.getFullName());
        history.setCreated_at(localDate);
        history.setNote(note);
        historyRepository.save(history);
    }
    @Override
    public boolean transferPerson(List<JsonObject> personList, Long idFamily) {
        Family family = familyRepository.findById(idFamily).orElse(null);
        if ((family==null) || (family.getIsDeleted()!=0)) {
            return false;
        }
        else {
            LocalDate localDate = LocalDate.now();
            for (int n=0; n<personList.size();n++){
                JsonObject personObject = personList.get(n);
                Long idPerson = Long.parseLong((String)personObject.get("idPerSon"));
                Person person = personRepository.findById(idPerson).orElse(null);
                if (person==null)
                    return false;
                Family sourceFamily = null;
                Relationship relationship = null;
                if (person.getRelationship().getId() == PersonServiceImpl.relationIdGoodman){
                    sourceFamily = person.getFamily();
                    relationship = person.getRelationship();
                }
                person.setFamily(family);
                person.setUpdated_at(localDate);
//                person.setRelationship(null);
                personRepository.save(person);

                if (sourceFamily!=null){
                    List<Person> personListFamilySource =
                            personRepository.
                                    findAllByFamilyAndIsDeletedOrderByIdDesc(sourceFamily,0);
                    if (personListFamilySource.size()==0){
                        sourceFamily.setIsDeleted(1);
                        sourceFamily.setDeleted_at(localDate);
                        familyRepository.save(sourceFamily);
                    }
                    else {
                        Person newGoodman = personListFamilySource.get(0);
                        newGoodman.setRelationship(relationship);
                        newGoodman.setUpdated_at(localDate);
                        personRepository.save(newGoodman);
                        sourceFamily.setGoodman(newGoodman.getFullName());
                        sourceFamily.setUpdated_at(localDate);
                    }
                }
                this.writeHistory(localDate, person, person.getFamily(),"Nhân Khẩu Chuyển Đi Hộ Khác Trong Tỉnh");
            }
            return true;
        }
    }

    @Override
    public void changeRelation(List<JsonObject> personList) {
        LocalDate localDate = LocalDate.now();
        HashMap<Long, Relationship> relationMap = this.getMapRelation();
        boolean checkGoodman = true;

        for (int n=0; n<personList.size();n++){
            JsonObject personObject = personList.get(n);
            long oldRelation = Long.parseLong((String)personObject.get("oldRelation"));
            long newRelation = Long.parseLong((String)personObject.get("newRelation"));
            if (oldRelation!=newRelation){
                Long idPerson = Long.parseLong((String)personObject.get("personId"));
                Person person = personRepository.findById(idPerson).orElse(null);
                if (newRelation == PersonServiceImpl.relationIdGoodman){
                    if (checkGoodman){
                        person.setRelationship(relationMap.get(newRelation));
                        person.setUpdated_at(localDate);
                        checkGoodman =false;
                        personRepository.save(person);
                        Family family = person.getFamily();
                        family.setGoodman(person.getFullName());
                        family.setUpdated_at(localDate);
                        familyRepository.save(family);
                    }
                }
                else {
                    if (person != null){
                        person.setRelationship(relationMap.get(newRelation));
                        person.setUpdated_at(localDate);
                        personRepository.save(person);
                    }
                }

            }
        }
    }

    @Override
    public void moveOut(List<JsonObject> personList, Long idFamily) {
        LocalDate localDate = LocalDate.now();
        Family family = familyRepository.findById(idFamily).orElse(null);
        if (family!=null){
            int numberPerson = personRepository.countAllByFamily(family);
            boolean checkGoodman = false,
                    writeHistory = true;
            for (int n=0; n<personList.size();n++){
                JsonObject personObject = personList.get(n);

                Long idPerson = Long.parseLong((String)personObject.get("idPerSon"));
                Person person = personRepository.findById(idPerson).orElse(null);
                if (person!=null){
                    if (person.getFamily().getId() == PersonServiceImpl.relationIdGoodman){
                        checkGoodman = true;
                    }
                    person.setUpdated_at(localDate);
                    person.setFamily(null);
                    personRepository.save(person);
                    this.writeHistory(localDate,person,family,"Nhân Khẩu Chuyển Đi Tỉnh Khác");
                }
            }
            if (numberPerson == personList.size()){
                family.setIsDeleted(1);
                family.setDeleted_at(localDate);
            }
            else if (checkGoodman){
                Person person = personRepository.findFirstByFamily(family);
                HashMap<Long ,Relationship> relationshipHashMap = this.getMapRelation();
                person.setRelationship(relationshipHashMap.get(PersonServiceImpl.relationIdGoodman));
                personRepository.save(person);
                family.setUpdated_at(localDate);
            }

            familyRepository.save(family);
        }

    }

    private HashMap<Long, Relationship> getMapRelation(){
        Iterable<Relationship> relationships = relationshipRepository.findAll();
        HashMap<Long ,Relationship> listRelation = new HashMap<>();
        for (Relationship relationship: relationships){
            listRelation.put(relationship.getId(), relationship);
        }
        return  listRelation;
    }

    @Override
    public Iterable<Person> getPerson(String record, String goodman,@Param("family_id") Long family_id) {
        return personRepository.getPerson(record, goodman,family_id);
    }

    @Override
    public Iterable<Person> getPersonByListId(List<Long> ids) {
        return personRepository.getPersonByListId(ids);
    }

    //    =====================================================================

    @Override
    public void editRelationship(Long family_id, Long relationship_id, Long id) {
        personRepository.editRelationship(family_id, relationship_id, id);
    }
    @Override
    public void rebornOnly(Long id) {
        personRepository.rebornOnly(id);
    }

    @Override
    public Iterable<Person> getOnly(String record) {
        return personRepository.getOnly(record);
    }

    @Override
    public void softDeleteOnly(LocalDate deleted_at, String deleted_by, Long family) {
    personRepository.softDeleteOnly(deleted_at,deleted_by,family);
    }

    @Override
    public Iterable<Person> idPerson(Long family_id) {
        return personRepository.idPerson(family_id);
    }

    @Override
    public String getDeadFullname(Long id) {
        return personRepository.getDeadFullname(id);
    }

    @Override
    public boolean edit(Person person) {
        Person sourcePerson = personRepository.findById(person.getId()).orElse(null);
        if (sourcePerson == null)
            return false;
        else {
            sourcePerson.setRelationship(person.getRelationship());
            sourcePerson.setBirthDay(person.getBirthDay());
            return true;
        }

    }

}