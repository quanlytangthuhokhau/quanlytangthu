package com.codegym.cms.service.impl;

import com.codegym.cms.model.City;
import com.codegym.cms.repository.CityRepository;
import com.codegym.cms.service.CityService;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

public class CityServiceImpl implements CityService {
    @Autowired
    private CityRepository cityRepository;

    @Override
    public City findById(Long id) {
        return cityRepository.findById(id).get();
    }

    @Override
    public Iterable<City> findAllByIsDeletedEquals(int isDeleted) {
        return cityRepository.findAllByIsDeletedEquals(isDeleted);
    }

    @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id) {
        cityRepository.softDelete(deleted_at, deleted_by, id);
    }


    @Override
    public void create(@Param("guild") String guild, @Param("idDistrict") Long idDistrict, @Param("district") String district, @Param("idCity") Long idCity, @Param("city") String city, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by) {
        cityRepository.create(guild, idDistrict, district, idCity, city, created_at, created_by);
    }

    @Override
    public void edit(@Param("guild") String guild, @Param("idDistrict") Long idDistrict, @Param("district") String district, @Param("idCity") Long idCity, @Param("city") String city, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id) {
        cityRepository.edit(guild, idDistrict, district, idCity, city, updated_at, updated_by, id);
    }

    @Override
    public List<String> city() {
        return cityRepository.city();
    }

    @Override
    public List<String> district(String city) {
        return cityRepository.district(city);
    }

    @Override
    public List<String> guild(String district) {
        return cityRepository.guild(district);
    }
}