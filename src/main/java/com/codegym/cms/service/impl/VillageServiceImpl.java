package com.codegym.cms.service.impl;
import com.codegym.cms.model.Village;
import com.codegym.cms.repository.VillageRepository;
import com.codegym.cms.service.VillageService;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;
import java.time.LocalDate;
public class VillageServiceImpl implements VillageService {
   @Autowired
   private VillageRepository villageRepository;

   @Override
   public Village findById(Long id){
       return villageRepository.findById(id).get();
}
   @Override
   public Iterable<Village> findAllByIsDeletedEquals(int isDeleted) {
       return villageRepository.findAllByIsDeletedEquals(isDeleted);
   }
   @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id){
       villageRepository.softDelete(deleted_at, deleted_by, id);
}


    @Override
    public void create(@Param("name") String name, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by){
       villageRepository.create(name, created_at, created_by);}
   @Override
    public void edit(@Param("name") String name, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long   id){
       villageRepository.edit(name, updated_at, updated_by, id);
 }
}