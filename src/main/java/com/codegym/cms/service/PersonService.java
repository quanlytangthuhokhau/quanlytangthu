package com.codegym.cms.service;

import com.codegym.cms.model.Family;
import com.codegym.cms.model.Person;
import com.codegym.cms.model.Relationship;
import com.github.cliftonlabs.json_simple.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.repository.query.Param;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import java.util.List;

public interface PersonService {
   Person findById(Long id);
   Iterable<Person> findAllByIsDeletedEquals(int isDeleted);
   void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id);


   void create(@Param("fullName") String fullName, @Param("relationship_id") Long relationship_id, @Param("sex") int sex, @Param("birthDay") LocalDate birthDay, @Param("idCard") String idCard, @Param("passport") String passport, @Param("nativeCountry") String nativeCountry, @Param("ethnic_id") Long ethnic_id, @Param("business") String business, @Param("phone") String phone, @Param("family_id") Long family_id, @Param("religion_id") Long religion_id, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by);


   void edit(@Param("fullName") String fullName, @Param("relationship_id") Long relationship_id, @Param("sex") int sex, @Param("birthDay") LocalDate birthDay, @Param("idCard") String idCard, @Param("passport") String passport, @Param("nativeCountry") String nativeCountry, @Param("ethnic_id") Long ethnic_id, @Param("business") String business, @Param("phone") String phone, @Param("family_id") Long family_id, @Param("religion_id") Long religion_id, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id );

   Iterable<Person> getPerson(@Param("record") String record,@Param("goodman") String goodman,@Param("family_id") Long family_id);
   Person findFirstPerson(Family family, Relationship relationship);
   List<Person> findPersonByFamily(Long idFamily, int page, int size);
   List<Person> findPersonByFamily(Long idFamily);
   int countPersonByFamily(Long idFamily);
   void createPerson(JsonObject person);
   boolean transferPerson(List<JsonObject> personList, Long idFamily);
   void changeRelation(List<JsonObject> personList);
   void moveOut(List<JsonObject> personList, Long idFamily);
   List<Integer> getId(@Param("record")String record,@Param("goodman")String goodman,@Param("fullName")String fullName);

   Iterable<Person> getIdPerson(@Param("record")String record);

   Iterable<Person> getDeadPerson(@Param("id")Long id);

   void reborn(@Param("id") Long id);

   void rebornOnly(@Param("id") Long id);

   Iterable<Person> getOnly(@Param("record")String record);

   void softDeleteOnly(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("family") Long family);

   Iterable<Person> idPerson(@Param("family_id")Long family_id);

   String getDeadFullname(@Param("id") Long id);
   boolean edit(Person person);
   //    =====================================================================
   Iterable<Person> getPersonByListId(@Param("ids") List<Long> ids);

   void editRelationship(@Param("family_id") Long family_id,@Param("relationship_id") Long relationship_id, @Param("id") Long id);

}
