package com.codegym.cms.model;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UserUpFile extends User {
    private CommonsMultipartFile[] fileDatas;
    public UserUpFile(){
    }
    public CommonsMultipartFile[] getFileDatas() {
        return fileDatas;
    }
    public void setFileDatas(CommonsMultipartFile[] fileDatas) {
        this.fileDatas = fileDatas;
    }

}
