package com.codegym.cms.repository;

import com.codegym.cms.model.City;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

public interface CityRepository extends PagingAndSortingRepository<City, Long> {

    Iterable<City> findAllByIsDeletedEquals(int isDeleted);

    @Transactional
    @Modifying
    @Query("UPDATE City b SET b.isDeleted = 1, b.deleted_at = :deleted_at, b.deleted_by = :deleted_by WHERE b.id=:id")
    void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id);


    @Transactional
    @Modifying
    @Query(value = "INSERT INTO City ( id, guild, idDistrict, district, idCity, city, created_at, created_by )" +
            "VALUES ((SELECT IFNULL((SELECT MAX(id) FROM City C) + 1, 1 )), :guild, :idDistrict, :district, :idCity, :city, :created_at, :created_by);", nativeQuery = true)
    void create(@Param("guild") String guild, @Param("idDistrict") Long idDistrict, @Param("district") String district, @Param("idCity") Long idCity, @Param("city") String city, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by);


    @Transactional
    @Modifying
    @Query(value = "UPDATE City SET guild = :guild, idDistrict = :idDistrict, district = :district, idCity = :idCity, city = :city, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id", nativeQuery = true)
    void edit(@Param("guild") String guild, @Param("idDistrict") Long idDistrict, @Param("district") String district, @Param("idCity") Long idCity, @Param("city") String city, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id);

    @Query(value = "SELECT distinct city FROM City WHERE isDeleted = 0", nativeQuery = true)
    List<String> city();

    @Query(value = "SELECT distinct district FROM City WHERE city = :city AND isDeleted = 0", nativeQuery = true)
    List<String> district(@Param("city") String city);

    @Query(value = "SELECT distinct guild FROM City WHERE district = :district AND isDeleted = 0", nativeQuery = true)
    List<String> guild(@Param("district") String district);
}
