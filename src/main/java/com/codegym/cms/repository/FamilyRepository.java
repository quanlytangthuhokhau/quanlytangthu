package com.codegym.cms.repository;

import com.codegym.cms.model.Family;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.codegym.cms.model.Person;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

public interface FamilyRepository extends PagingAndSortingRepository<Family, Long> {

    Iterable<Family> findAllByIsDeletedEquals(int isDeleted);

    Page<Family> findAllByIsDeletedEquals(int isDeleted, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE Family b SET b.isDeleted = 1, b.deleted_at = :deleted_at, b.deleted_by = :deleted_by WHERE b.id=:id")
    void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id);


    @Transactional
    @Modifying
    @Query(value = "INSERT INTO Family ( id, page, record, goodman, village_id, created_at, created_by )" +
            "VALUES ((SELECT IFNULL((SELECT MAX(id) FROM Family C) + 1, 1 )), :page, :record, :goodman, :village_id, :created_at, :created_by);", nativeQuery = true)
    void create(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by);


    @Transactional
    @Modifying
    @Query(value = "UPDATE Family SET page = :page, record = :record, goodman = :goodman, village_id = :village_id, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id", nativeQuery = true)
    void edit(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id);

    @Query(value = "SELECT MAX(id) FROM Family WHERE page = :page AND record = :record AND goodman = :goodman AND village_id = :village_id AND isDeleted = 0", nativeQuery = true)
    Long findIdFamily(@Param("page") String page, @Param("record") String record, @Param("goodman") String goodman, @Param("village_id") Long village_id);

    @Query(value = "SELECT DISTINCT page FROM Family " +
            "WHERE isDeleted=0")
    List<String> getAllPage();

    Iterable<Family> findAllByPageAndIsDeletedEquals(String page, int isDeleted);

    @Query(value = "SELECT * FROM Family WHERE page = :page AND record = :record", nativeQuery = true)
    Family family(@Param("page") String page, @Param("record") String record);


    @Query(value="SELECT * FROM Family where record=:record and isDeleted=0",nativeQuery = true)
    Iterable<Family> getAllGoodMan(@Param("record")String record);

    @Query(value="SELECT fullName FROM Person where family_id=:family_id",nativeQuery = true)
    List<String> getAllFamily(@Param("family_id")Long id);

    Family findFirstByRecordAndIsDeleted(String record, int isDeleted);
    Page<Family> findAllByPageAndIsDeleted(String page, int isDeleted, Pageable pageable);

    @Query(value = "SELECT DISTINCT record FROM Family " +
            "WHERE isDeleted=0")
    List<String> getAllRecord();

    @Query(value="SELECT fullName FROM Person inner join Family on Person.family_id = Family.id where record = :record and Person.isDeleted=0",nativeQuery = true)
    List<String> getAllFamily(@Param("record")String record);
    Page<Family> findAllByRecordAndIsDeleted(String record, int isDeleted, Pageable pageable);

    @Query(value="SELECT family_id FROM Person group by family_id having count(family_id)=1",nativeQuery = true)
    List<Long> getIdDeadOnly();

    @Query(value="SELECT record FROM Family WHERE id = :id",nativeQuery = true)
    String getDeadOnly(@Param("id")Long id);

    @Transactional
    @Modifying
    @Query("UPDATE Family f SET f.isDeleted = 0 WHERE f.id=:id")
    void rebornFamily(@Param("id") Long id);


    Page<Family> findAllByPageAndRecordAndIsDeleted(String page, String record, int isDeleted, Pageable pageable);
    @Query(value="SELECT goodman FROM Family WHERE id = :id",nativeQuery = true)
    String getDeadFamily(@Param("id") Long id);

    @Query(value = "SELECT IFNULL((SELECT MAX(CAST(record AS UNSIGNED)) FROM Family) + 1, 1)", nativeQuery = true)
    String getMaxRecord();

    @Query(value = "SELECT IFNULL((SELECT MAX(CAST(page AS UNSIGNED)) FROM Family) + 1, 1)", nativeQuery = true)
    String getMaxPage();

    @Query(value = "SELECT * FROM Family Where page = :page AND record = :record AND goodman = :goodman AND village_id = :village_id AND isDeleted = 0", nativeQuery = true)
    Family getFamilyByFull(@Param("page") String page,@Param("record") String record,@Param("goodman") String goodman,@Param("village_id") Long village_id);

}
