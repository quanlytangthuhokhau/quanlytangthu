package com.codegym.cms.repository;

import com.codegym.cms.model.History;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface HistoryRepository extends PagingAndSortingRepository<History, Long> {
    List<History> getAllByIsDeletedOrderByIdDesc(int isDeleted, Pageable pageable);
    int countAllByIsDeleted(int isDeleted);
}
