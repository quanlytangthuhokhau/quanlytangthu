package com.codegym.cms.repository; 
import com.codegym.cms.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    Iterable<User> findAllByIsDeletedEquals(int isDeleted);

    @Transactional
    @Modifying
    @Query("UPDATE User b SET b.isDeleted = 1, b.deleted_at = :deleted_at, b.deleted_by = :deleted_by WHERE b.id=:id")
    void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id);


    @Transactional
    @Modifying
    @Query(value = "INSERT INTO User ( id, username, password, enabled, roles, picture, created_at, created_by )" + 
               "VALUES ((SELECT IFNULL((SELECT MAX(id) FROM User C) + 1, 1 )), :username, :password, :enabled, :roles, :picture, :created_at, :created_by);", nativeQuery = true)
    void create(@Param("username") String username, @Param("password") String password, @Param("enabled") int enabled, @Param("roles") String roles, @Param("picture") String picture, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by);


    @Transactional
    @Modifying
    @Query(value = "UPDATE User SET username = :username, password = :password, enabled = :enabled, roles = :roles, picture = :picture, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id", nativeQuery = true)
     void edit(@Param("username") String username, @Param("password") String password, @Param("enabled") int enabled, @Param("roles") String roles, @Param("picture") String picture, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id);

    @Query(value = "select id from User where username=:username", nativeQuery = true)
    Long idSign(@Param("username") String username);

    @Transactional
    @Modifying
    @Query(value = "UPDATE User SET username = :username, password = :password,picture = :picture, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id", nativeQuery = true)
    void editUser(@Param("username") String username,@Param("password") String password, @Param("picture") String picture, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id);

}
