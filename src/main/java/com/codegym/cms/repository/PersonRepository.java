package com.codegym.cms.repository;
import com.codegym.cms.model.Family;
import com.codegym.cms.model.Person;
import com.codegym.cms.model.Relationship;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {

    Iterable<Person> findAllByIsDeletedEquals(int isDeleted);

    @Transactional
    @Modifying
    @Query("UPDATE Person b SET b.isDeleted = 1, b.deleted_at = :deleted_at, b.deleted_by = :deleted_by WHERE b.id=:id")
    void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id);


    @Transactional
    @Modifying
    @Query(value = "INSERT INTO Person ( id, fullName, relationship_id, sex, birthDay, idCard, passport, nativeCountry, ethnic_id, business, phone, family_id, religion_id, created_at, created_by )" +
               "VALUES ((SELECT IFNULL((SELECT MAX(id) FROM Person C) + 1, 1 )), :fullName, :relationship_id, :sex, :birthDay, :idCard, :passport, :nativeCountry, :ethnic_id, :business, :phone, :family_id, :religion_id, :created_at, :created_by);", nativeQuery = true)
    void create(@Param("fullName") String fullName, @Param("relationship_id") Long relationship_id, @Param("sex") int sex, @Param("birthDay") LocalDate birthDay, @Param("idCard") String idCard, @Param("passport") String passport, @Param("nativeCountry") String nativeCountry, @Param("ethnic_id") Long ethnic_id, @Param("business") String business, @Param("phone") String phone, @Param("family_id") Long family_id, @Param("religion_id") Long religion_id, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by);


    @Transactional
    @Modifying
    @Query(value = "UPDATE Person SET fullName = :fullName, relationship_id = :relationship_id, sex = :sex, birthDay = :birthDay, idCard = :idCard, passport = :passport, nativeCountry = :nativeCountry, ethnic_id = :ethnic_id, business = :business, phone = :phone, family_id = :family_id, religion_id = :religion_id, updated_at = :updated_at, updated_by = :updated_by WHERE id = :id", nativeQuery = true)
     void edit(@Param("fullName") String fullName, @Param("relationship_id") Long relationship_id, @Param("sex") int sex, @Param("birthDay") LocalDate birthDay, @Param("idCard") String idCard, @Param("passport") String passport, @Param("nativeCountry") String nativeCountry, @Param("ethnic_id") Long ethnic_id, @Param("business") String business, @Param("phone") String phone, @Param("family_id") Long family_id, @Param("religion_id") Long religion_id, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id);

    @Query(value = "select Person.id from Person inner join Family on Person.family_id = Person.id where record=:record and goodman = :goodman and fullName = :fullName ", nativeQuery = true)
    List<Integer> getId(@Param("record") String record, @Param("goodman") String goodman, @Param("fullName") String fullName);

    //    @Query(value="SELECT Person.* FROM Person inner join Family on Person.family_id = Family.id where record = :record and Person.isDeleted=0 and Person.relationship_id <>1",nativeQuery = true)
//    Iterable<Person> getIdPerson(@Param("record")String record);
    @Query(value = "SELECT Person.* FROM Person inner join Family on Person.family_id = Family.id where record = :record and Person.isDeleted=0", nativeQuery = true)
    Iterable<Person> getIdPerson(@Param("record") String record);

    @Query(value = "SELECT * FROM Person WHERE id = :id", nativeQuery = true)
    Iterable<Person> getDeadPerson(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query("UPDATE Person b SET b.isDeleted = 0 WHERE b.id=:id")
    void reborn(@Param("id") Long id);
    @Query(value = "SELECT p.* FROM Family f JOIN Person p ON f.id = p.family_id WHERE f.record = :record AND f.goodman = :goodman AND p.family_id = :family_id AND f.isDeleted = 0 AND p.isDeleted = 0", nativeQuery = true)
    Iterable<Person> getPerson(@Param("record") String record,@Param("goodman") String goodman,@Param("family_id") Long family_id);
    Person findFirstByFamilyAndRelationshipAndIsDeleted(Family family, Relationship relationship, int isDeleted);

    Page<Person> findAllByFamilyAndIsDeletedOrderByIdDesc(Family family, int isDeleted, Pageable pageable);
    List<Person> findAllByFamilyAndIsDeletedOrderByIdDesc(Family family, int isDeleted);
    int countAllByFamily(Family family);
    Person findFirstByFamily(Family family);
    @Query(value = "SELECT p FROM Person p WHERE p.birthDay>=:fromBirthday AND p.birthDay <=:toBirthday AND p.isDeleted=0")
    List<Person> findByBirthDay(@Param("fromBirthday") LocalDate fromBirthday, @Param("toBirthday") LocalDate toBirthday);
    @Query(value = "SELECT p FROM Person p WHERE p.birthDay>=:fromBirthday AND p.birthDay <=:toBirthday " +
            "AND p.sex = :gender AND p.isDeleted=0")
    List<Person> findByGenderAndBirthDay(@Param("fromBirthday") LocalDate fromBirthday,
                                         @Param("toBirthday") LocalDate toBirthday,
                                         @Param("gender") int gender);

    @Transactional
    @Modifying
    @Query("UPDATE Person b SET b.isDeleted = 0 WHERE b.family.id=:id")
    void rebornOnly(@Param("id") Long id);

    @Query(value = "SELECT Person.* FROM Person inner join Family on Person.family_id = Family.id where record = :record and Person.isDeleted=0", nativeQuery = true)
    Iterable<Person> getOnly(@Param("record") String record);

    @Transactional
    @Modifying
    @Query("UPDATE Person b SET b.isDeleted = 1, b.deleted_at = :deleted_at, b.deleted_by = :deleted_by WHERE b.family.id=:family")
    void softDeleteOnly(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("family") Long family);

    @Query(value = "SELECT * FROM Person WHERE family_id = :family_id", nativeQuery = true)
    Iterable<Person> idPerson(@Param("family_id") Long family_id);

    @Query(value = "SELECT fullName FROM Person WHERE id = :id", nativeQuery = true)
    String getDeadFullname(@Param("id") Long id);

    //    =====================================================================
    @Query(value = "SELECT * FROM Person WHERE id IN (:ids) AND isDeleted = 0", nativeQuery = true)
    Iterable<Person> getPersonByListId(@Param("ids") List<Long> ids);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Person SET family_id = :family_id, relationship_id = :relationship_id WHERE id = :id AND isDeleted = 0", nativeQuery = true)
    void editRelationship(@Param("family_id") Long family_id,@Param("relationship_id") Long relationship_id, @Param("id") Long id);


}
